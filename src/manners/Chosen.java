package manners;

public class Chosen {
	int id;

	String name;

	String hobby;

	public Chosen(int id, String n, String h) {
		this.id = id;
		this.name = n;
		this.hobby = h;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String toString() {
		return "(chosen (id " + id + ") (name " + name + ") (hobby " + hobby
				+ "))";
	}
}
