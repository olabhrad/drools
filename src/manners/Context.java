package manners;

public class Context {
	String state;

	public Context(String s) {
		this.state = s;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String toString() {
		return "(context (state " + state + "))";
	}
}
