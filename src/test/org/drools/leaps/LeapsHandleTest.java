package test.org.drools.leaps;

import junit.framework.TestCase;

public class LeapsHandleTest extends TestCase {

    /*
     * Test method for 'leaps.LeapsFactHandle.getId()'
     */
    public void testGetId() {
        LeapsHandle handle = new LeapsHandle(123456789, new String ("test124"));
        assertEquals(handle.getId(), 123456789);
        assertEquals(handle.object, "test124");
    }


}
