package org.drools.leaps.conflict;

import java.util.Comparator;

import junit.framework.TestCase;

import org.drools.leaps.LeapsHandle;

public class LeapsLoadOrderConflictResolverTest extends TestCase {

    private Comparator resolver = LeapsLoadOrderConflictResolver.getInstance();

    /*
     * Test method for
     * 'org.drools.leaps.conflict.LeapsLoadOrderConflictResolver.compare(Object,
     * Object)'
     */
    public void testCompare() {
        // compare is inverted for sorting in leaps tables
        assertEquals(resolver.compare(new LeapsHandle(1, "1"), new LeapsHandle(
                2, "2")), 1);
    }

}
