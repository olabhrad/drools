package org.drools.leaps.conflict;

import java.util.Comparator;
import junit.framework.TestCase;

import org.drools.leaps.LeapsCondition;
import org.drools.leaps.LeapsConsequence;
import org.drools.leaps.LeapsDeclaration;
import org.drools.leaps.LeapsRuleHandle;
import org.drools.leaps.rule.LeapsRule;

public class LeapsRuleSalienceConflictResolverTest extends TestCase {

    private Comparator resolver = LeapsRuleSalienceConflictResolver.getInstance();
    private LeapsDeclaration ruleDecl;

    private LeapsRule rule1;
    private LeapsRuleHandle ruleHandle1;

    private LeapsRule rule2;
    private LeapsRuleHandle ruleHandle2;

    protected void setUp() throws Exception {
        super.setUp();
        rule1 = new LeapsRule("rule1");
        ruleDecl = rule1.addParameterDeclaration("object", Object.class);
        rule1.addCondition(new LeapsCondition() {
            public boolean isAllowed() throws Exception {
                return true;
            }

            public LeapsDeclaration[] getRequiredDeclarations() {
                return new LeapsDeclaration[] { ruleDecl };
            }
        });
        rule1.setConsequence(new LeapsConsequence() {
            public void invoke() throws Exception {
            }
        });
        // setting salience
        rule1.setSalience(1);
        ruleHandle1= new LeapsRuleHandle(0, rule1, 0);
        
        rule2 = new LeapsRule("rule2");
        ruleDecl = rule2.addParameterDeclaration("object", Object.class);
        rule2.addCondition(new LeapsCondition() {
            public boolean isAllowed() throws Exception {
                return true;
            }

            public LeapsDeclaration[] getRequiredDeclarations() {
                return new LeapsDeclaration[] { ruleDecl };
            }
        });
        rule2.setConsequence(new LeapsConsequence() {
            public void invoke() throws Exception {
            }
        });
        // setting salience
        rule2.setSalience(2);
        ruleHandle2= new LeapsRuleHandle(0, rule2, 0);

    }

    /*
     * Test method for
     * 'org.drools.leaps.conflict.LeapsRuleSalienceConflictResolver.compare(Object,
     * Object)'
     */
    public void testCompare() {
        // compare is inverted for sorting in leaps tables
        assertEquals(resolver.compare(ruleHandle1, ruleHandle2), 1);
    }

}
