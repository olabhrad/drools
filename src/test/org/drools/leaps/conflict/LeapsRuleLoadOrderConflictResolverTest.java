package org.drools.leaps.conflict;

import java.util.Comparator;
import junit.framework.TestCase;

import org.drools.leaps.LeapsCondition;
import org.drools.leaps.LeapsConsequence;
import org.drools.leaps.LeapsDeclaration;
import org.drools.leaps.LeapsRuleHandle;
import org.drools.leaps.rule.LeapsRule;

public class LeapsRuleLoadOrderConflictResolverTest extends TestCase {


    public static void main(String[] args) throws Throwable {
        LeapsRuleLoadOrderConflictResolverTest test = new LeapsRuleLoadOrderConflictResolverTest();
        test.runTest();
    }

    private Comparator resolver = LeapsRuleLoadOrderConflictResolver
            .getInstance();

    private LeapsDeclaration ruleDecl;

    private LeapsRule rule;

    private LeapsRuleHandle ruleHandle11;

    private LeapsRuleHandle ruleHandle12;

    private LeapsRule rule2;

    private LeapsRuleHandle ruleHandle2;

    protected void setUp() throws Exception {
        super.setUp();

        rule = new LeapsRule("rule");
        ruleDecl = rule.addParameterDeclaration("object1", Object.class);
        ruleDecl = rule.addParameterDeclaration("object2", Object.class);
        rule.addCondition(new LeapsCondition() {
            public boolean isAllowed() throws Exception {
                return true;
            }

            public LeapsDeclaration[] getRequiredDeclarations() {
                return new LeapsDeclaration[] { ruleDecl };
            }
        });
        rule.setConsequence(new LeapsConsequence() {
            public void invoke() throws Exception {
            }
        });
        // setting salience
        ruleHandle11 = new LeapsRuleHandle(0, rule, 0);
        ruleHandle12 = new LeapsRuleHandle(0, rule, 1);

        rule2 = new LeapsRule("rule2");
        ruleDecl = rule2.addParameterDeclaration("object", Object.class);
        rule2.addCondition(new LeapsCondition() {
            public boolean isAllowed() throws Exception {
                return true;
            }

            public LeapsDeclaration[] getRequiredDeclarations() {
                return new LeapsDeclaration[] { ruleDecl };
            }
        });
        rule2.setConsequence(new LeapsConsequence() {
            public void invoke() throws Exception {
            }
        });
        // setting salience
        ruleHandle2 = new LeapsRuleHandle(0, rule2, 0);
    }

    /*
     * Test method for
     * 'org.drools.leaps.conflict.LeapsRuleSalienceConflictResolver.compare(Object,
     * Object)'
     */
    public void testCompareDifferentCEIds() {
        // compare is inverted for sorting in leaps tables
        assertEquals(resolver.compare(ruleHandle11, ruleHandle12), 1);
    }

    /*
     * Test method for
     * 'org.drools.leaps.conflict.LeapsRuleSalienceConflictResolver.compare(Object,
     * Object)'
     */
    public void testCompareEverythingTheSame() {
        assertEquals(resolver.compare(ruleHandle11, ruleHandle2), 0);
    }

}
