package test.org.drools.leaps;


import org.drools.leaps.rule.LeapsRule;

import junit.framework.TestCase;

public class LeapsRuleHandleTest extends TestCase {

	private LeapsRule rule;

	private LeapsRuleHandle handle;

	protected void setUp() {
		rule = new LeapsRule("test name");
		handle = new LeapsRuleHandle(54, rule, 66);
	}

	/*
	 * Test method for 'org.drools.leaps.LeapsRuleHandle.getRule()'
	 */
	public void testGetRule() {
		assertEquals(rule, handle.getRule());
	}

	/*
	 * Test method for 'org.drools.leaps.LeapsRuleHandle.getCePosition()'
	 */
	public void testGetCePosition() {
		assertEquals(66, handle.getCePosition());

	}

}
