package org.drools.leaps.util;

import java.util.Comparator;

import org.drools.leaps.LeapsHandle;
import org.drools.leaps.conflict.LeapsRuleSalienceConflictResolver;

import junit.framework.TestCase;

public class LeapsTableRecordTest extends TestCase {

    public void testConstractor() {
		String object = new String("test object");
		LeapsHandle handle = new LeapsHandle(12, object);
		LeapsTableRecord record = new LeapsTableRecord(handle);
		assertEquals(object, record.object);
	}

}
