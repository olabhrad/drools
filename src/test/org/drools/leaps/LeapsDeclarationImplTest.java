package test.org.drools.leaps;

import org.drools.leaps.LeapsDeclarationImpl;

import junit.framework.TestCase;

public class LeapsDeclarationImplTest extends TestCase {

    private LeapsDeclarationImpl decl1;
    private LeapsDeclarationImpl decl2;
    private LeapsDeclarationImpl decl3;
    private LeapsDeclarationImpl declLike1;
    
    protected void setUp() { 
        decl1 = new LeapsDeclarationImpl("identifier1", String.class, 2);
        decl2 = new LeapsDeclarationImpl("identifier2", String.class, 1);
        decl3 = new LeapsDeclarationImpl("identifier3", String.class, 8);
        declLike1 = new LeapsDeclarationImpl("identifier1", String.class, 2);
    }
    /*
     * Test method for 'leaps.LeapsDeclarationImpl.getType()'
     */
    public void testGetType() {
        assertEquals(String.class, decl1.getType());
    }

    /*
     * Test method for 'leaps.LeapsDeclarationImpl.getIdentifier()'
     */
    public void testGetIdentifier() {
        assertEquals(decl3.getIdentifier(), "identifier3");
    }

    /*
     * Test method for 'leaps.LeapsDeclarationImpl.getIndex()'
     */
    public void testGetIndex() {
        assertEquals(decl2.getIndex(), 1);
    }

    /*
     * Test method for 'leaps.LeapsDeclarationImpl.getValue()'
     */
    public void testGetValue() {

    }

    /*
     * Test method for 'leaps.LeapsDeclarationImpl.getFactHandle()'
     */
    public void testGetFactHandle() {

    }

    /*
     * Test method for 'leaps.LeapsDeclarationImpl.equals(Object)'
     */
    public void testEqualsObject() {
        assertEquals(decl1, declLike1);
    }

}
