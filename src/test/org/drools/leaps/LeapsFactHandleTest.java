package test.org.drools.leaps;

import junit.framework.TestCase;

public class LeapsFactHandleTest extends TestCase {

    private LeapsRuleBase ruleBase;
    
    protected void setUp() { 
        ruleBase = new LeapsRuleBase();
    }
    /*
     * Test method for 'leaps.LeapsFactHandle.getId()'
     */
    public void testGetId() {
        LeapsWorkingMemory memory = ruleBase.newWorkingMemory();
        LeapsFactHandle fh1 = memory.assertObject("object1");
        assertEquals(fh1.getId(), memory.runningCounter);
    }

    /*
     * Test method for 'leaps.LeapsFactHandle.equals(Object)'
     */
    public void testEqualsObject() {
        // they equal on id, am not sure how to simulate it yet
    }

}
