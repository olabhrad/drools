package test.manners;

public class LastSeat {
	int seat;

	public LastSeat(int s) {
		seat = s;
	}

	public int getSeat() {
		return seat;
	}

	public void setSeat(int seat) {
		this.seat = seat;
	}

	public String toString() {
		return "(last_seat (seat " + seat + "))";
	}
}
