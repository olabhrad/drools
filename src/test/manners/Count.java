package test.manners;

public class Count {
	int c;

	public Count(int c) {
		this.c = c;
	}

	public int getC() {
		return c;
	}

	public void setC(int id) {
		this.c = id;
	}

	public String toString() {
		return "(count (c " + c + "))";
	}
}
