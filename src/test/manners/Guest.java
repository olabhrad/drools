package test.manners;

public class Guest {
	String name;

	String sex;

	String hobby;

	public Guest(String n, String s, String h) {
		name = n;
		sex = s;
		hobby = h;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String toString() {
		return "(guest (name " + name + ") (sex " + sex + ") (hobby " + hobby
				+ "))";
	}
}
