package test.manners;

public class Seating {
	int id;

	int pid;

	String name1;

	String name2;

	int seat1;

	int seat2;

	String pathDone;

	public Seating(int id, int pid, String name1, String name2, int seat1,
			int seat2, String pathDone) {
		this.id = id;
		this.pid = pid;
		this.name1 = name1;
		this.name2 = name2;
		this.seat1 = seat1;
		this.seat2 = seat2;
		this.pathDone = pathDone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name) {
		this.name1 = name;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public int getSeat1() {
		return seat1;
	}

	public void setSeat1(int seat1) {
		this.seat1 = seat1;
	}

	public int getSeat2() {
		return seat2;
	}

	public void setSeat2(int seat2) {
		this.seat2 = seat2;
	}

	public String getPathDone() {
		return pathDone;
	}

	public void setPathDone(String hobby) {
		this.pathDone = hobby;
	}

	public String toString() {
		return "(seating (id " + id + ") (pid " + pid + ") (name1 " + name1
				+ ") (name2 " + name2 + ") (seat1 " + seat1 + ") (seat2 "
				+ seat2 + ") (pathDone " + pathDone + "))";
	}

}
