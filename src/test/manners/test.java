package test.manners;

import org.drools.leaps.LeapsRuleBase;
import org.drools.leaps.LeapsWorkingMemory;

import test.manners.Facts;
import test.manners.Rules;

public class test {
    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

        System.out.println("LEAPS Ms. Manners test bed ...");
        long timeStamp = System.currentTimeMillis();
        LeapsRuleBase ruleBase = new LeapsRuleBase();
        LeapsWorkingMemory memory = ruleBase.newWorkingMemory();
        // memory.addEventListener(new LeapsDebugWorkingMemoryEventListener());
        /** instantiating facts */
        // Facts.generateFor4Guest(memory);
        // Facts.generateFor16Guest(memory);
        // Facts.generateFor64Guest(memory);
      //  Facts.generateFor128Guest(memory);
        // Facts.generateFor256Guest(memory);
        Facts.generateRdfFacts(memory);
        /** instantiate rules */
        Rules.generate(memory, ruleBase.newNewLeapsRuleSet("manners"));

        memory.fireAllRules();
        System.out.println("Elapsed time "
                + ((System.currentTimeMillis() - timeStamp) / 1000) + " sec.");

    }
}
