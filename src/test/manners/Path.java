package test.manners;

public class Path {
	int id;

	String name;

	int seat;

	public Path(int id, String name, int seat) {
		this.id = id;
		this.name = name;
		this.seat = seat;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSeat() {
		return seat;
	}

	public void setSeat(int seat) {
		this.seat = seat;
	}

	public String toString() {
		return "(path (id " + id + ") (name " + name + ") (seat " + seat + "))";
	}
}
