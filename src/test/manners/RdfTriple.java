package test.manners;


public class RdfTriple {
	
    private String subject;
    
    /** The predicate element of the pattern */
    private String predicate;
    
    /** The object element of the pattern */
    private String object;
    
    public RdfTriple(String s,String p,String o){
    	setSubject(s);
    	setPredicate(p);
    	setObject(o);
    	
    }

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getPredicate() {
		return predicate;
	}

	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	

}
