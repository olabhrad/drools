package test.manners;

import java.util.ArrayList;

import ie.tcd.cs.nembes.coror.leaps.CororLeapsRule;
import manners.Chosen;
import manners.Context;

import org.drools.leaps.*;
import org.drools.leaps.rule.*;

public class Rules {
	public static void generate(LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet) throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
//		Rules.generateAssignFirstSeat(workingMemory, ruleSet);
//		Rules.generateFindSeating(workingMemory, ruleSet);
//		Rules.generatePathDone(workingMemory, ruleSet);
//		Rules.generateMakePath(workingMemory, ruleSet);
//		Rules.generateContinue(workingMemory, ruleSet);
//		Rules.generateAreWeDone(workingMemory, ruleSet);
//		Rules.generatePrintResults(workingMemory, ruleSet);
		Rules.generateRdfTest(workingMemory, ruleSet);
	}
	private static CororLeapsRule generateRdfTest(
			LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet) throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		
		final LeapsWorkingMemory memory = workingMemory;
		
		CororLeapsRule assignFirstSeatRule = new CororLeapsRule("rdfs9");
		
		final LeapsDeclaration Decl1 = assignFirstSeatRule
				.addParameterDeclaration("trip1", RdfTriple.class);
		
		final LeapsDeclaration Decl2 = assignFirstSeatRule
				.addParameterDeclaration("trip2", RdfTriple.class);
		
		// condition
		assignFirstSeatRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				RdfTriple trip1 = (RdfTriple) Decl1.getValue();
				RdfTriple trip2 = (RdfTriple) Decl2.getValue();
				System.out.println(trip1.getSubject() + " - " +trip2.getObject() );
				return trip1.getPredicate().equals("http://www.w3.org/2000/01/rdf-schema#subClassOf")
						&& trip1.getSubject().equals(trip2.getObject());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(Decl1);l.add(Decl2);
				return l;
			}
		}
		);
		assignFirstSeatRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				RdfTriple trip1 = (RdfTriple) Decl1.getValue();
				RdfTriple trip2 = (RdfTriple) Decl2.getValue();
				//System.out.println(trip1.getObject() + " - " +trip2.getSubject() );
				return trip2.getPredicate().equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(Decl1);l.add(Decl2);
				return l;
			}
		}
		);
		/**
		 * negative conditions
		 */
		/**
		 * consequence
		 */
		assignFirstSeatRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("success");
				RdfTriple trip1 = (RdfTriple) Decl1.getValue();
				RdfTriple trip2 = (RdfTriple) Decl2.getValue();
				System.out.println(trip2.getSubject()+", "+","+trip1.getObject());
			/*	Guest guest = (Guest) guestDecl.getValue();
				Count count = (Count) countDecl.getValue();

				memory.assertObject(new Seating(count.getC(), 0, guest
						.getName(), guest.getName(), 1, 1, "yes"));
				memory.assertObject(new Path(count.getC(), guest.getName(), 1));
				memory.modifyFact(countDecl.getFactHandle(), new Count(
						count.getC() + 1));
				System.out.println("seat 1 " + guest.getName() + " "
						+ guest.getName() + " 1 " + count.getC() + " 0 1");
				memory.modifyFact(contextDecl.getFactHandle(),
						new Context("assign_seats"));*/
			}
		});
		ruleSet.addRule(assignFirstSeatRule);
		return assignFirstSeatRule;
	}

	/*
	 * (defrule assign_first_seat ?ctxt <- (context (state start)) (guest (name
	 * ?n)) ?count <- (count (c ?c)) => (assert (seating (seat1 1) (name1 ?n)
	 * (name2 ?n) (seat2 1) (id ?c) (pid 0) (path_done yes))) (assert (path (id
	 * ?c) (name ?n) (seat 1))) (modify ?count (c (+ ?c 1))) (printout t seat " "
	 * 1 " " ?n " " ?n " " 1 " " ?c " " 0 " " 1 crlf) (modify ?ctxt (state
	 * assign_seats)))
	 */
	private static CororLeapsRule generateAssignFirstSeat(
			LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet) throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		
		final LeapsWorkingMemory memory = workingMemory;
		
		CororLeapsRule assignFirstSeatRule = new CororLeapsRule("AssignFirstSeat");
		
		final LeapsDeclaration contextDecl = assignFirstSeatRule
				.addParameterDeclaration("context", Context.class);
		
		final LeapsDeclaration guestDecl = assignFirstSeatRule
				.addParameterDeclaration("guest", Guest.class);
		
		final LeapsDeclaration countDecl = assignFirstSeatRule
				.addParameterDeclaration("count", Count.class);
		// condition
		assignFirstSeatRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Context context = (Context) contextDecl.getValue();
				return "start".equals(context.getState());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		}
		);
		/**
		 * negative conditions
		 */
		/**
		 * consequence
		 */
		assignFirstSeatRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("success assignFirstSeatRule");
				Guest guest = (Guest) guestDecl.getValue();
				Count count = (Count) countDecl.getValue();

				memory.assertObject(new Seating(count.getC(), 0, guest
						.getName(), guest.getName(), 1, 1, "yes"));
				memory.assertObject(new Path(count.getC(), guest.getName(), 1));
				memory.modifyFact(countDecl.getFactHandle(), new Count(
						count.getC() + 1));
				System.out.println("seat 1 " + guest.getName() + " "
						+ guest.getName() + " 1 " + count.getC() + " 0 1");
				memory.modifyFact(contextDecl.getFactHandle(),
						new Context("assign_seats"));
			}
		});
		ruleSet.addRule(assignFirstSeatRule);
		return assignFirstSeatRule;
	}

	// (defrule find_seating
	// ?ctxt <- (context (state assign_seats))
	// (seating (seat1 ?seat1) (seat2 ?seat2) (name2 ?n2) (id ?id) (pid ?pid)
	// (path_done yes))
	// (guest (name ?n2) (sex ?s1) (hobby ?h1))
	// (guest (name ?g2) (sex ~?s1) (hobby ?h1))
	// ?count <- (count (c ?c))
	// (not (path (id ?id) (name ?g2)))
	// (not (chosen (id ?id) (name ?g2) (hobby ?h1)))
	// =>
	// (assert (seating (seat1 ?seat2) (name1 ?n2) (name2 ?g2) (seat2 (+ ?seat2 1))
	// (id ?c)
	// (pid ?id) (path_done no)))
	// (assert (path (id ?c) (name ?g2) (seat ( + ?seat2 1))))
	// (assert (chosen (id ?id) (name ?g2) (hobby ?h1)))
	// (modify ?count (c (+ ?c 1)))
	// (printout t seat " " ?seat2 " " ?n2 " " ?g2 crlf)
	// (modify ?ctxt (state make_path)))

	private static CororLeapsRule generateFindSeating(LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet)
			throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		final LeapsWorkingMemory memory = workingMemory;
		CororLeapsRule findSeatingRule = new CororLeapsRule("FindSeating");
		final LeapsDeclaration contextDecl = findSeatingRule
				.addParameterDeclaration("context", Context.class);
		final LeapsDeclaration seatingDecl = findSeatingRule
				.addParameterDeclaration("seatign", Seating.class);
		final LeapsDeclaration guest1Decl = findSeatingRule
				.addParameterDeclaration("guest1", Guest.class);
		final LeapsDeclaration guest2Decl = findSeatingRule
				.addParameterDeclaration("guest2", Guest.class);
		final LeapsDeclaration countDecl = findSeatingRule
				.addParameterDeclaration("count", Count.class);
		// condition
		findSeatingRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Context context = (Context) contextDecl.getValue();
				return "assign_seats".equals(context.getState());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		});
		findSeatingRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				return "yes".equals(seating.getPathDone());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		});
		findSeatingRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				Guest guest1 = (Guest) guest1Decl.getValue();
				return guest1.getName().equals(seating.getName2());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(seatingDecl);l.add(guest1Decl);
				return l;
			}
		});
		findSeatingRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Guest guest1 = (Guest) guest1Decl.getValue();
				Guest guest2 = (Guest) guest2Decl.getValue();
				return !guest1.getSex().equals(guest2.getSex())
						&& guest1.getHobby().equals(guest2.getHobby());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(guest1Decl);l.add(guest2Decl);
				return l;
			}
		});
		/**
		 * negative conditions
		 */
		findSeatingRule.addNegativeCondition(new LeapsNegativeCondition(
				Path.class) {
			public boolean isAllowed(Object path) throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				Guest guest2 = (Guest) guest2Decl.getValue();
				return ((Path) path).getId() == seating.getId()
						&& ((Path) path).getName().equals(guest2.getName());
			}
		});
		findSeatingRule.addNegativeCondition(new LeapsNegativeCondition(
				Chosen.class) {
			public boolean isAllowed(Object chosen) throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				Guest guest2 = (Guest) guest2Decl.getValue();
				return ((Chosen) chosen).getId() == seating.getId()
						&& ((Chosen) chosen).getName().equals(guest2.getName())
						&& ((Chosen) chosen).getHobby().equals(
								guest2.getHobby());
			}
		});
		/**
		 * consequence
		 */
		findSeatingRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("success findSeatingRule");
				Seating seating = (Seating) seatingDecl.getValue();
				Guest guest1 = (Guest) guest1Decl.getValue();
				Guest guest2 = (Guest) guest2Decl.getValue();
				Count count = (Count) countDecl.getValue();
				memory.assertObject(new Seating(count.getC(), seating.getId(),
						guest1.getName(), guest2.getName(), seating.getSeat2(),
						seating.getSeat2() + 1, "no"));
				memory.assertObject(new Path(count.getC(), guest2.getName(),
						seating.getSeat2() + 1));
				memory.assertObject(new Chosen(seating.getId(), guest2
						.getName(), guest2.getHobby()));
				memory.modifyFact(countDecl.getFactHandle(), new Count(
						count.getC() + 1));
				memory.modifyFact(contextDecl.getFactHandle(),
						new Context("make_path"));
			}
		});
		ruleSet.addRule(findSeatingRule);
		return findSeatingRule;
	}

	// (defrule make_path
	// (context (state make_path))
	// (seating (id ?id) (pid ?pid) (path_done no))
	// (path (id ?pid)(name ?n1) (seat ?s))
	// (not (path (id ?id) (name ?n1)))
	// =>
	// (assert (path (id ?id) (name ?n1) (seat ?s))))
	private static CororLeapsRule generateMakePath(LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet)
			throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		final LeapsWorkingMemory memory = workingMemory;
		CororLeapsRule makePathRule = new CororLeapsRule("MakePath");
		final LeapsDeclaration contextDecl = makePathRule.addParameterDeclaration(
				"context", Context.class);
		final LeapsDeclaration seatingDecl = makePathRule.addParameterDeclaration(
				"seating", Seating.class);
		final LeapsDeclaration pathDecl = makePathRule.addParameterDeclaration(
				"path", Path.class);
		// condition
		makePathRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Context context = (Context) contextDecl.getValue();
				return "make_path".equals(context.getState());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		});
		makePathRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				return "no".equals(seating.getPathDone());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(seatingDecl);
				return l;
			}
		});
		makePathRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				Path path = (Path) pathDecl.getValue();
				return path.getId() == seating.getPid();
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(seatingDecl);l.add(pathDecl);
				return l;
			}
		});
		/**
		 * negative conditions
		 */
		makePathRule.addNegativeCondition(new LeapsNegativeCondition(Path.class) {
			public boolean isAllowed( Object path)
					throws Exception {
				// System.out.println("negative make path : " + path + "\n" +
				// token);
				Seating seating = (Seating) seatingDecl.getValue();
				Path pathA = (Path) pathDecl.getValue();
				return ((Path) path).getId() == seating.getId()
						&& ((Path) path).getName().equals(pathA.getName());
			}
		});
		/**
		 * consequence
		 */
		makePathRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("success makePathRule");
				Seating seating = (Seating) seatingDecl.getValue();
				Path path = (Path) pathDecl.getValue();
				memory.assertObject(new Path(seating.getId(), path.getName(),
						path.getSeat()));
			}
		});
		ruleSet.addRule(makePathRule);
		return makePathRule;
	}
	// (defrule path_done
	// ?ctxt <- (context (state make_path))
	// ?seat <- (seating (path_done no))
	// =>
	// (modify ?seat (path_done yes))
	// (modify ?ctxt (state check_done)))
	private static CororLeapsRule generatePathDone(LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet)
			throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		final LeapsWorkingMemory memory = workingMemory;
		CororLeapsRule pathDoneRule = new CororLeapsRule("PathDone");
		final LeapsDeclaration contextDecl = pathDoneRule.addParameterDeclaration(
				"context", Context.class);
		final LeapsDeclaration seatingDecl = pathDoneRule.addParameterDeclaration(
				"seating", Seating.class);
		pathDoneRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				// System.out.println("checking path done");
				Context context = (Context) contextDecl.getValue();
				return "make_path".equals(context.getState());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		});
		pathDoneRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				return "no".equals(seating.getPathDone());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(seatingDecl);
				return l;
			}
		});
		/**
		 * negative conditions
		 */
		/**
		 * consequence
		 */
		pathDoneRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("success pathDoneRule");
				Seating seating = (Seating) seatingDecl.getValue();
				memory.modifyFact(seatingDecl.getFactHandle(),
						new Seating(seating.getId(), seating.getPid(), seating
								.getName1(), seating.getName2(), seating
								.getSeat1(), seating.getSeat2(), "yes"));
				memory.modifyFact(contextDecl.getFactHandle (),
						new Context("check_done"));
			}
		});
		ruleSet.addRule(pathDoneRule);
		return pathDoneRule;
	}
	// (defrule continue
	// ?ctxt <- (context (state check_done))
	// =>
	// (modify ?ctxt (state assign_seats)))
	private static CororLeapsRule generateContinue(LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet)
			throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		final LeapsWorkingMemory memory = workingMemory;
		CororLeapsRule continueRule = new CororLeapsRule("Continue");
		final LeapsDeclaration contextDecl = continueRule.addParameterDeclaration(
				"context", Context.class);
		continueRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Context context = (Context) contextDecl.getValue();
				return "check_done".equals(context.getState());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		});
		/**
		 * negative conditions
		 */
		/**
		 * consequence
		 */
		continueRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("success continueRule");
				memory.modifyFact(contextDecl.getFactHandle(),
						new Context("assign_seats"));
			}
		});
		ruleSet.addRule(continueRule);
		return continueRule;
	}
	// (defrule are_we_done
	// ?ctxt <- (context (state check_done))
	// (last_seat (seat ?l_seat))
	// (seating (seat2 ?l_seat))
	// =>
	// (printout t crlf "Yes, we are done!!" crlf)
	// (modify ?ctxt (state print_results))
	// )
	private static CororLeapsRule generateAreWeDone(LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet)
			throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		final LeapsWorkingMemory memory = workingMemory;
		CororLeapsRule areWeDoneRule = new CororLeapsRule("AreWeDone");
		final LeapsDeclaration contextDecl = areWeDoneRule.addParameterDeclaration(
				"context", Context.class);
		final LeapsDeclaration lastSeatDecl = areWeDoneRule
				.addParameterDeclaration("last_seat", LastSeat.class);
		final LeapsDeclaration seatingDecl = areWeDoneRule.addParameterDeclaration(
				"seating", Seating.class);
		areWeDoneRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Context context = (Context) contextDecl.getValue();
				return "check_done".equals(context.getState());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		});
		areWeDoneRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				LastSeat lastSeat = (LastSeat) lastSeatDecl.getValue();
				Seating seating = (Seating) seatingDecl.getValue();
				return seating.getSeat2() == lastSeat.getSeat();
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(seatingDecl);l.add(lastSeatDecl);
				return l;
			}
		});
		/**
		 * negative conditions
		 */
		/**
		 * consequence
		 */
		areWeDoneRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("Yes, we are done!!");
				memory.modifyFact(contextDecl.getFactHandle(),
						new Context("print_results"));
				// memory.dump();
				/// System.exit(0);
			}
		});
		ruleSet.addRule(areWeDoneRule);
		return areWeDoneRule;
	}
	// (defrule print_results
	// (context (state print_results))
	// (seating (id ?id) (seat2 ?s2))
	// (last_seat (seat ?s2))
	// ?path <- (path (id ?id) (name ?n) (seat ?s))
	// =>
	// (retract ?path)
	// (printout t ?n " " ?s crlf))
	private static CororLeapsRule generatePrintResults(
			LeapsWorkingMemory workingMemory, LeapsRuleSet ruleSet) throws LeapsInvalidRuleException, LeapsDuplicateRuleNameException {
		final LeapsWorkingMemory memory = workingMemory;
		CororLeapsRule printResultsRule = new CororLeapsRule("PrintResults");
		final LeapsDeclaration contextDecl = printResultsRule
				.addParameterDeclaration("context", Context.class);
		final LeapsDeclaration seatingDecl = printResultsRule
				.addParameterDeclaration("seating", Seating.class);
		final LeapsDeclaration lastSeatDecl = printResultsRule
				.addParameterDeclaration("last_seat", LastSeat.class);
		final LeapsDeclaration pathDecl = printResultsRule.addParameterDeclaration(
				"path", Path.class);
		printResultsRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Context context = (Context) contextDecl.getValue();
				return "print_results".equals(context.getState());
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(contextDecl);
				return l;
			}
		});
		printResultsRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				LastSeat lastSeat = (LastSeat) lastSeatDecl.getValue();
				Seating seating = (Seating) seatingDecl.getValue();
				return seating.getSeat2() == lastSeat.getSeat();
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(lastSeatDecl);l.add(seatingDecl);
				return l;
			}
		});
		printResultsRule.addCondition(new LeapsCondition() {
			public boolean isAllowed() throws Exception {
				Seating seating = (Seating) seatingDecl.getValue();
				Path path = (Path) pathDecl.getValue();
				return seating.getId() == path.getId();
			}
			public ArrayList<LeapsDeclaration> getRequiredDeclarations() {
				ArrayList<LeapsDeclaration> l = new ArrayList<LeapsDeclaration>();
				l.add(seatingDecl);l.add(pathDecl);
				return l;
			}
		});
		/**
		 * negative conditions
		 */
		/**
		 * consequence
		 */
		printResultsRule.setConsequence(new LeapsConsequence() {
			public void invoke() throws Exception {
				System.out.println("success printResultsRule");
				Path path = (Path) pathDecl.getValue();
				System.out.println(path.getName() + " " + path.getSeat());
				memory.retractFact(pathDecl.getFactHandle ());
			}
		});
		ruleSet.addRule(printResultsRule);
		return printResultsRule;
	}}
