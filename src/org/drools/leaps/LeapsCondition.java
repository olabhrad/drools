package org.drools.leaps;

import java.util.ArrayList;

/**
 * Rule's positive condition
 * 
 * @author ab
 *
 */
public interface LeapsCondition {
	/**
	 * @return
	 * @throws Exception
	 */
	boolean isAllowed() throws Exception;
    /**
     * @return
     */
 //   public LeapsDeclaration[] getRequiredDeclarations();
    public ArrayList<LeapsDeclaration> getRequiredDeclarations();
}
