package org.drools.leaps;

import org.drools.leaps.LeapsDeclaration;
import org.drools.leaps.util.LeapsTableIterator;

/**
 * 
 */
public class LeapsDeclarationImpl implements LeapsDeclaration {

    /** The identifier for the variable. */
    private final String identifier;

    private final Class type;

    /** The index within a rule. */
    private final int index;

    private LeapsTableIterator iterator = null;

    public void setIterator(LeapsTableIterator iterator) {
        this.iterator = iterator;
    }

    public LeapsDeclarationImpl(String identifier, Class c, int order) {
        this.type = c;
        this.identifier = identifier;
        this.index = order;
    }

    /**
     * Retrieve the <code>Type</code>.
     * 
     * @return The object-type.
     */
    public Class getType() {
        return this.type;
    }

    /**
     * Retrieve the variable's identifier.
     * 
     * @return The variable's identifier.
     */
    public String getIdentifier() {
        return this.identifier;
    }

    public int getIndex() {
        return this.index;
    }

    public Object getValue() {
        return ((LeapsFactHandle) this.iterator.current()).object;
    }

    public LeapsFactHandle getFactHandle() {
        return (LeapsFactHandle) this.iterator.current();
    }

    public String toString() {
        return "[Declaration: " + this.identifier + "]";
    }

    public int hashCode() {
        return this.index;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        LeapsDeclarationImpl other = (LeapsDeclarationImpl) object;

        return this.index == other.index
                && this.identifier.equals(other.identifier)
                && this.type.equals(other.type);
    }
}
