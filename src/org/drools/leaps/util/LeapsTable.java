package org.drools.leaps.util;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.SortedMap;


public abstract class LeapsTable {
	protected LeapsTableRecord headRecord = new LeapsTableRecord(null);

	TreeMap map;

	protected void setMap(TreeMap map) {
		this.map = map;
	}

    public LeapsTable() {
	}

	/**
	 * add work by adding element to the first position so we can use iteration
	 * from position
	 */
	public void add(Object object) {
		LeapsTableRecord newRecord = new LeapsTableRecord(object);
		if (this.map.isEmpty()) {
			this.headRecord = newRecord;
		} else {
			SortedMap bufMap = this.map.headMap(object);
            if (!bufMap.isEmpty()) {
				LeapsTableRecord bufRec = (LeapsTableRecord) map.get(bufMap
						.lastKey());
				if (bufRec.right != null) {
					bufRec.right.left = newRecord;
				}
				newRecord.right = bufRec.right;
				bufRec.right = newRecord;
				newRecord.left = bufRec;

			} else {
				this.headRecord.left = newRecord;
				newRecord.right = this.headRecord;
				this.headRecord = newRecord;
			}
		}
		this.map.put(object, newRecord);
	}

	public void remove(Object object) {
		LeapsTableRecord record = (LeapsTableRecord) this.map.get(object);
		if (record != null) {
			if (record == this.headRecord) {
				if (record.right != null) {
					this.headRecord = record.right;
				} else {
					this.headRecord = new LeapsTableRecord(null);
				}
			} else {
				// left
				LeapsTableRecord leftRecord = record.left;
				LeapsTableRecord rightRecord = record.right;
				leftRecord.right = rightRecord;
				if (rightRecord != null) {
					rightRecord.left = leftRecord;
				}
			}
			// map
			record.left = null;
			record.right = null;
			this.map.remove(object);
		}
	}

	public Object getHeadObject() {
		return this.headRecord.object;
	}

	public LeapsTableIterator iterator() throws LeapsTableOutOfBoundException {
		return this.iterator(this.headRecord.object);
	}

	public LeapsTableIterator iterator(Object object)
			throws LeapsTableOutOfBoundException {
		return this.iterator(object, object);
	}

	public LeapsTableIterator iterator(Object objectAtStart,
			Object objectAtPosition) throws LeapsTableOutOfBoundException {
		return this.iterator(objectAtStart, objectAtPosition, null);
	}

	public LeapsTableIterator iterator(Object objectAtStart,
			Object objectAtPosition, Object objectAtEnd)
			throws LeapsTableOutOfBoundException {
		return new LeapsTableIterator(this.map, objectAtStart,
				objectAtPosition, objectAtEnd);
	}

	public void dump() throws LeapsTableOutOfBoundException {
		for (Iterator it = this.iterator(); it.hasNext();) {
			System.out.println(it.next());
		}
	}

	public void dump(Object object) throws LeapsTableOutOfBoundException {
		for (Iterator it = this.iterator(object); it.hasNext();) {
			System.out.println(it.next());
		}
	}

	public void dump(Object object, Object objectAtPosition)
			throws LeapsTableOutOfBoundException {
		for (Iterator it = this.iterator(object, objectAtPosition); it
				.hasNext();) {
			System.out.println(it.next());
		}
		System.out.println("the same table but after being reset()");
		Iterator it = this.iterator(object, objectAtPosition);
		((LeapsTableIterator) it).reset();
		for (; it.hasNext();) {
			System.out.println(it.next());
		}
	}

	public void dump(Object object, Object objectAtPosition, Object objectAtEnd)
			throws LeapsTableOutOfBoundException {
		for (Iterator it = this.iterator(object, objectAtPosition, objectAtEnd); it
				.hasNext();) {
			System.out.println(it.next());
		}
		System.out.println("the same table but after being reset()");
		Iterator it = this.iterator(object, objectAtPosition, objectAtEnd);
		((LeapsTableIterator) it).reset();
		for (; it.hasNext();) {
			System.out.println(it.next());
		}
	}
}
