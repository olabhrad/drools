package org.drools.leaps.util;

public class LeapsTableOutOfBoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LeapsTableOutOfBoundException() {
		super();
	}

	public LeapsTableOutOfBoundException(String msg) {
		super(msg);
	}

	public LeapsTableOutOfBoundException(Exception ex) {
		super(ex);
	}
}
