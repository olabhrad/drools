package org.drools.leaps.util;

import java.util.Iterator;
import java.util.SortedMap;


public class LeapsTableIterator implements Iterator {
	private boolean empty;

	private LeapsTableRecord startRecord = new LeapsTableRecord(null);

	private LeapsTableRecord currentRecord = startRecord;

	private LeapsTableRecord lastRecord = null;

	public LeapsTableIterator(Object object) {
		this.currentRecord = new LeapsTableRecord(object);
		this.lastRecord = this.currentRecord;
		//
		this.startRecord.right = currentRecord;
		this.currentRecord = startRecord;
	}

	public LeapsTableIterator(SortedMap map, Object objectAtStart,
			Object objectAtPosition, Object objectAtEnd)
			throws LeapsTableOutOfBoundException {
		if (!map.isEmpty()
				&& (objectAtEnd == null || (objectAtEnd != null && map
						.comparator().compare(objectAtStart, objectAtEnd) <= 0))) {
			SortedMap iteratorMap;
			SortedMap rewindMap;
			if (map.comparator().compare(objectAtStart, objectAtPosition) > 0) {
				throw new LeapsTableOutOfBoundException(
						"object at position is out of upper bound");
			}
			if (objectAtEnd == null) {
				iteratorMap = map.tailMap(objectAtStart);
			} else {
				// it does not pick up the object at the end but this is fine
				// because we need it for negative iterations where lower
				// boundary
				// is retracted fact
				iteratorMap = map.subMap(objectAtStart, objectAtEnd);
			}

            if (!iteratorMap.isEmpty()) {
				this.startRecord.right = (LeapsTableRecord) iteratorMap
						.get(iteratorMap.firstKey());
				this.lastRecord = (LeapsTableRecord) iteratorMap.get(iteratorMap.lastKey());
			}
			//
			if (map.comparator().compare(objectAtStart, objectAtPosition) == 0) {
				this.currentRecord = this.startRecord;
			} else {
				// rewind
				if (!iteratorMap.isEmpty()) {
					rewindMap = iteratorMap.tailMap(objectAtPosition);

                    if (!rewindMap.isEmpty()) {
						LeapsTableRecord bufRecord = ((LeapsTableRecord) rewindMap
								.get(rewindMap.firstKey()));
						// no changes if it's starting object
						if (map.comparator()
								.compare(this.startRecord.right.object,
										bufRecord.object) < 0) {
							this.currentRecord = bufRecord.left;
						} else {
							this.currentRecord = this.startRecord;
						}
					} else {
						this.reset();
					}
				}
			}
		}
        // setting empty indicator
		this.empty = (this.startRecord.right == null);
	}

	public boolean isEmpty() {
		return this.empty;
	}

	public void reset() {
		this.currentRecord = this.startRecord;
	}

	public boolean hasNext() {
		if (!this.empty) {
			return this.currentRecord != this.lastRecord;
			// return currentRecord.right != null;
		} else {
			return false;
		}
	}

	public Object next() {
		this.currentRecord = this.currentRecord.right;
		return this.currentRecord.object;
	}

	public Object current() {
		return this.currentRecord.object;
	}

	public Object peekNext() {
		return this.currentRecord.right.object;
	}

	public void remove() {
	}
}
