package org.drools.leaps.util;

public class LeapsTableRecord {
	// left neigbor
	LeapsTableRecord left;

	// left neigbor
	LeapsTableRecord right;

	// content of the record
	Object object;

	LeapsTableRecord(Object o) {
		this.left = null;
		this.right = null;
		this.object = o;
	}

}
