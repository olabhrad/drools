package org.drools.leaps;

public class LeapsNoMatchesFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LeapsNoMatchesFoundException(String msg) {
		super(msg);
	}

}
