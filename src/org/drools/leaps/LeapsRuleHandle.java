package org.drools.leaps;

import ie.tcd.cs.nembes.coror.leaps.CororLeapsRule;


/**
 * 
 * @author ab class container for objects asserted into the system
 */
public class LeapsRuleHandle extends LeapsHandle {
	// ce position
	private final int cePosition;

	public LeapsRuleHandle(long id, CororLeapsRule rule, int cePosition) {
		super(id, rule);
		this.cePosition = cePosition;
	}

	public CororLeapsRule getRule() {
		return (CororLeapsRule)this.object;
	}

	public int getCePosition() {
		return cePosition;
	}

	public boolean equals(Object that) {
		if (this == that)
			return true;
		if (!(that instanceof LeapsRuleHandle))
			return false;
		return (this.getRule().getSalience() == ((LeapsRuleHandle) that)
				.getRule().getSalience())
				&& (((this.getRule().getNumberOfParameterDeclarations() + this
						.getRule().getNegativeConditions().size())) == (((LeapsRuleHandle) that)
						.getRule().getNumberOfParameterDeclarations() + ((LeapsRuleHandle) that)
						.getRule().getNegativeConditions().size()))
				&& this.getId() == ((LeapsRuleHandle) that).getId()
				&& (this.getCePosition() == ((LeapsRuleHandle) that)
						.getCePosition());
	}

	public String toString() {
		return "R-" + this.getId() + " \"" + this.getRule().toString() + "\" [pos - "
				+ this.cePosition + "]";
	}
}
