package org.drools.leaps.event;

import org.drools.leaps.LeapsWorkingMemory;
import org.drools.leaps.LeapsNegativeCondition;
import org.drools.leaps.LeapsToken;

public class LeapsNegativeConditionTestedEvent extends LeapsWorkingMemoryEvent {
    private final LeapsNegativeCondition condition;

    private final LeapsToken token;

    private final boolean passed;

    public LeapsNegativeConditionTestedEvent(LeapsWorkingMemory workingMemory,
            LeapsNegativeCondition condition, LeapsToken token, boolean passed) {
        super(workingMemory);
        this.condition = condition;
        this.token = token;
        this.passed = passed;
    }

    public LeapsNegativeCondition getCondition() {
        return this.condition;
    }

    public LeapsToken getToken() {
        return this.token;
    }

    public boolean getPassed() {
        return this.passed;
    }

    public String toString() {
        return "[ConditionTested: condition=" + this.condition + "; passed="
                + this.passed + "; tuple=" + this.token + "]";
    }
}
