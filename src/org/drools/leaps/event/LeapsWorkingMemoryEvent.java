package org.drools.leaps.event;

import java.util.EventObject;

import org.drools.leaps.LeapsWorkingMemory;

public class LeapsWorkingMemoryEvent extends EventObject {
    public LeapsWorkingMemoryEvent(LeapsWorkingMemory workingMemory) {
        super(workingMemory);
    }

    public final LeapsWorkingMemory getWorkingMemory() {
        return (LeapsWorkingMemory) getSource();
    }
}
