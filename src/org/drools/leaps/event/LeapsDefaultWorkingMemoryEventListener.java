package org.drools.leaps.event;

public class LeapsDefaultWorkingMemoryEventListener
    implements
    LeapsWorkingMemoryEventListener
{
    public LeapsDefaultWorkingMemoryEventListener()
    {
        // intentionally left blank
    }

    public void objectAsserted(LeapsObjectAssertedEvent event)
    {
        // intentionally left blank
    }

    public void objectRetracted(LeapsObjectRetractedEvent event)
    {
        // intentionally left blank
    }

    public void conditionTested(LeapsConditionTestedEvent event)
    {
        // intentionally left blank
    }

    public void negativeConditionTested(LeapsNegativeConditionTestedEvent event)
    {
        // intentionally left blank
    }

    public void ruleFired(LeapsRuleFiredEvent event)
    {
        // intentionally left blank
    }
}
