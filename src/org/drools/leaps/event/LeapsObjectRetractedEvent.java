package org.drools.leaps.event;

import org.drools.leaps.LeapsFactHandle;
import org.drools.leaps.LeapsWorkingMemory;

public class LeapsObjectRetractedEvent extends LeapsWorkingMemoryEvent {
    private final LeapsFactHandle handle;

    public LeapsObjectRetractedEvent(LeapsWorkingMemory workingMemory,
            LeapsFactHandle handle) {
        super(workingMemory);
        this.handle = handle;
    }

    public LeapsFactHandle getFactHandle() {
        return this.handle;
    }

    public Object getObject() {
        return this.handle.getObject();
    }

    public String toString() {
        return "[ObjectRetracted: handle=" + this.handle + "]";
    }
}
