package org.drools.leaps.event;

import ie.tcd.cs.nembes.coror.leaps.CororLeapsRule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.drools.leaps.LeapsCondition;
import org.drools.leaps.LeapsFactHandle;
import org.drools.leaps.LeapsNegativeCondition;
import org.drools.leaps.LeapsToken;
import org.drools.leaps.LeapsWorkingMemory;

/**
 * @author <a href="mailto:simon@redhillconsulting.com.au">Simon Harris </a>
 */
public class LeapsWorkingMemoryEventSupport implements Serializable {
    private final List listeners = new ArrayList();

    private final LeapsWorkingMemory workingMemory;

    public LeapsWorkingMemoryEventSupport(LeapsWorkingMemory workingMemory) {
        this.workingMemory = workingMemory;
    }

    public void addEventListener(LeapsWorkingMemoryEventListener listener) {
        if (!this.listeners.contains(listener)) {
            this.listeners.add(listener);
        }
    }

    public void removeEventListener(LeapsWorkingMemoryEventListener listener) {
        this.listeners.remove(listener);
    }

    public List getEventListeners() {
        return Collections.unmodifiableList(this.listeners);
    }

    public int size() {
        return this.listeners.size();
    }

    public boolean isEmpty() {
        return this.listeners.isEmpty();
    }

    public void fireObjectAsserted(LeapsFactHandle handle) {
        if (this.listeners.isEmpty()) {
            return;
        }

        LeapsObjectAssertedEvent event = new LeapsObjectAssertedEvent(
                this.workingMemory, handle);

        for (int i = 0, size = this.listeners.size(); i < size; i++) {
            ((LeapsWorkingMemoryEventListener) this.listeners.get(i))
                    .objectAsserted(event);
        }
    }

    public void fireObjectRetracted(LeapsFactHandle handle) {
        if (this.listeners.isEmpty()) {
            return;
        }

        LeapsObjectRetractedEvent event = new LeapsObjectRetractedEvent(
                this.workingMemory, handle);

        for (int i = 0, size = this.listeners.size(); i < size; i++) {
            ((LeapsWorkingMemoryEventListener) this.listeners.get(i))
                    .objectRetracted(event);
        }
    }

    public void firePositiveConditionTested(LeapsCondition condition,
            LeapsToken token, boolean result) {
        if (this.listeners.isEmpty()) {
            return;
        }

        LeapsConditionTestedEvent event = new LeapsConditionTestedEvent(
                this.workingMemory, condition, token, result);

        for (int i = 0, size = this.listeners.size(); i < size; i++) {
            ((LeapsWorkingMemoryEventListener) this.listeners.get(i))
                    .conditionTested(event);
        }
    }

    public void fireNegativeConditionTested(CororLeapsRule rule,
            LeapsNegativeCondition condition, LeapsToken token, boolean result) {
        if (this.listeners.isEmpty()) {
            return;
        }

        LeapsNegativeConditionTestedEvent event = new LeapsNegativeConditionTestedEvent(
                this.workingMemory, condition, token, result);

        for (int i = 0, size = this.listeners.size(); i < size; i++) {
            ((LeapsWorkingMemoryEventListener) this.listeners.get(i))
                    .negativeConditionTested(event);
        }
    }

    public void fireRuleFired(LeapsToken token) {
        if (this.listeners.isEmpty()) {
            return;
        }

        LeapsRuleFiredEvent event = new LeapsRuleFiredEvent(this.workingMemory,
                token);

        for (int i = 0, size = this.listeners.size(); i < size; i++) {
            ((LeapsWorkingMemoryEventListener) this.listeners.get(i))
                    .ruleFired(event);
        }
    }
}
