package org.drools.leaps.event;

import org.drools.leaps.LeapsWorkingMemory;
import org.drools.leaps.LeapsToken;

public class LeapsRuleFiredEvent extends LeapsWorkingMemoryEvent {
    private LeapsToken token;

    public LeapsRuleFiredEvent(LeapsWorkingMemory workingMemory,
            LeapsToken token) {
        super(workingMemory);

        this.token = token;
    }

    public LeapsToken getToken() {
        return this.token;
    }

    public String toString() {
        return "[Fired: "
                + this.token.getCurrentRuleHandle().getRule().getName()
                + "; token=" + this.token + "]";
    }
}
