package org.drools.leaps.event;

import org.drools.leaps.LeapsFactHandle;
import org.drools.leaps.LeapsWorkingMemory;

public class LeapsObjectAssertedEvent extends LeapsWorkingMemoryEvent {
    private final LeapsFactHandle handle;

    public LeapsObjectAssertedEvent(LeapsWorkingMemory workingMemory,
            LeapsFactHandle handle) {
        super(workingMemory);
        this.handle = handle;
    }

    public LeapsFactHandle getFactHandle() {
        return this.handle;
    }

    public Object getObject() {
        return this.handle.getObject();
    }

    public String toString() {
        return "[ObjectAsserted: handle=" + this.handle + "]";
    }
}
