package org.drools.leaps.event;

import org.drools.leaps.LeapsWorkingMemory;
import org.drools.leaps.LeapsCondition;
import org.drools.leaps.LeapsToken;

public class LeapsConditionTestedEvent extends LeapsWorkingMemoryEvent {
    private final LeapsCondition condition;

    private final LeapsToken token;

    private final boolean passed;

    public LeapsConditionTestedEvent(LeapsWorkingMemory workingMemory,
            LeapsCondition condition, LeapsToken token,
            boolean passed) {
        super(workingMemory);
        this.condition = condition;
        this.token = token;
        this.passed = passed;
    }

    public LeapsCondition getCondition() {
        return this.condition;
    }

    public LeapsToken getToken() {
        return this.token;
    }

    public boolean getPassed() {
        return this.passed;
    }

    public String toString() {
        return "[ConditionTested: condition=" + this.condition + "; passed="
                + this.passed + "; tuple=" + this.token + "]";
    }
}
