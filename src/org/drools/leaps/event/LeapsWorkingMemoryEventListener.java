package org.drools.leaps.event;

import java.util.EventListener;

public interface LeapsWorkingMemoryEventListener extends EventListener {
    void objectAsserted(LeapsObjectAssertedEvent event);

    void objectRetracted(LeapsObjectRetractedEvent event);

    void conditionTested(LeapsConditionTestedEvent event);

    void negativeConditionTested(LeapsNegativeConditionTestedEvent event);

    void ruleFired(LeapsRuleFiredEvent event);
}
