package org.drools.leaps.event;


public class LeapsDebugWorkingMemoryEventListener
    implements
    LeapsWorkingMemoryEventListener
{
    public LeapsDebugWorkingMemoryEventListener()
    {
        // intentionally left blank
    }

    public void objectAsserted(LeapsObjectAssertedEvent event)
    {
        System.err.println( event );
    }

    public void objectRetracted(LeapsObjectRetractedEvent event)
    {
        System.err.println( event );
    }

    public void conditionTested(LeapsConditionTestedEvent event)
    {
        System.err.println( event );
    }

    public void negativeConditionTested(LeapsNegativeConditionTestedEvent event)
    {
        System.err.println( event );
    }
    public void ruleFired(LeapsRuleFiredEvent event)
    {
        System.err.println( event );
    }
}
