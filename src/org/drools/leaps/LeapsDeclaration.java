package org.drools.leaps;


/**
 * 
 * this interface for external use outside of Leaps package
 * 
 * @author
 * 
 */
public interface LeapsDeclaration {
	public Object getValue();

	public LeapsFactHandle getFactHandle();

}
