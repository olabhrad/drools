package org.drools.leaps;

import ie.tcd.cs.nembes.coror.leaps.CororLeapsRule;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.drools.leaps.conflict.LeapsDefaultConflictResolver;
import org.drools.leaps.rule.LeapsRuleSet;

public class LeapsRuleBase {

    // to store rules added just as addRule rather than as a part of ruleSet
    private final static String defaultRuleSet = "___default___rule___set___";

    private Hashtable ruleSets;

    private Map applicationData;

    //private RuleBaseContext ruleBaseContext;

    private LeapsConflictResolver conflictResolver;

    /* @todo: replace this with a weak HashSet */
    private final transient Map workingMemories;

    /** Special value when adding to the underlying map. */
    private static final Object PRESENT = new Object();

    public LeapsRuleBase() {
        this(LeapsDefaultConflictResolver.getInstance());
    }

    public LeapsRuleBase(LeapsConflictResolver conflictResolver) {
        this.conflictResolver = conflictResolver;
        this.workingMemories = new WeakHashMap();
        this.ruleSets = new Hashtable();
        this.ruleSets.put(defaultRuleSet,
                new LeapsRuleSet(defaultRuleSet, this));
    }

    /**
     * Create a new <code>WorkingMemory</code> session for this
     * <code>RuleBase</code>.
     * 
     * <p>
     * The created <code>WorkingMemory</code> uses the default conflict
     * resolution strategy.
     * </p>
     * 
     * @see WorkingMemory
     * @see org.drools.conflict.DefaultConflictResolver
     * 
     * @return A newly initialized <code>WorkingMemory</code>.
     */
    /**
     * @see RuleBase
     */
    public LeapsWorkingMemory newWorkingMemory() {
        return newWorkingMemory(true);
    }

    /**
     * @see RuleBase
     */
    public LeapsWorkingMemory newWorkingMemory(boolean keepReference) {
        LeapsWorkingMemory workingMemory = new LeapsWorkingMemory(this);
        if (keepReference) {
            this.workingMemories.put(workingMemory, PRESENT);
        }
        return workingMemory;
    }

    void disposeWorkingMemory(LeapsWorkingMemory workingMemory) {
        this.workingMemories.remove(workingMemory);
    }

    public Set getWorkingMemories() {
        return this.workingMemories.keySet();
    }

    /**
     * Retrieve the <code>LeapsConflictResolver</code>.
     * 
     * @return The conflict resolution strategy.
     */
    /**
     * @see RuleBase
     */
    public LeapsConflictResolver getConflictResolver() {
        return this.conflictResolver;
    }

    /**
     * rule set operations
     * 
     */
    public LeapsRuleSet newNewLeapsRuleSet(String name) {
        LeapsRuleSet ruleSet;

        if (this.ruleSets.containsKey(name)) {
            ruleSet = (LeapsRuleSet) this.ruleSets.get(name);
        } else {
            ruleSet = new LeapsRuleSet(name, this);
        }
        return ruleSet;
    }

    public List getRuleSets() {
        return Collections.list(this.ruleSets.elements());
    }

    // adding rules
    public void addRule(LeapsRuleSet ruleSet, CororLeapsRule rule) {
        for (Iterator it = this.getWorkingMemories().iterator(); it.hasNext();) {
            ((LeapsWorkingMemory) it.next()).addRule(rule);
        }
    }

    // adding standalone rule
    public void addRule(CororLeapsRule rule) {
        this.addRule((LeapsRuleSet) this.ruleSets
                .get(LeapsRuleBase.defaultRuleSet), rule);
    }

    public Map getApplicationData() {
        return this.applicationData;
    }

   // public RuleBaseContext getRuleBaseContext() {
   //     return this.ruleBaseContext;
   // }

    // fact operations for all working memories
    public void assertObject(Object object) {
        for (Iterator it = this.getWorkingMemories().iterator(); it.hasNext();) {
            ((LeapsWorkingMemory) it.next()).assertObject(object);
        }
    }

    public void retractObject(Object object) throws LeapsNoSuchFactHandleException {
        for (Iterator it = this.getWorkingMemories().iterator(); it.hasNext();) {
            ((LeapsWorkingMemory) it.next()).retractObject(object);
        }
    }

    public void modifyObject(Object object) throws LeapsNoSuchFactHandleException {
        for (Iterator it = this.getWorkingMemories().iterator(); it.hasNext();) {
            ((LeapsWorkingMemory) it.next()).modifyObject(object);
        }
    }

}
