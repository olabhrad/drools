package org.drools.leaps;

import ie.tcd.cs.nembes.coror.leaps.CororLeapsRule;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.drools.leaps.event.LeapsWorkingMemoryEventListener;
import org.drools.leaps.event.LeapsWorkingMemoryEventSupport;
import org.drools.leaps.util.LeapsTableOutOfBoundException;

public class LeapsWorkingMemory implements PropertyChangeListener {
    private final static Class rootClass = Object.class;

    String lock = new String("lock");

    long runningCounter;

    /** The <code>RuleBase</code> with which this memory is associated. */
    private final LeapsRuleBase ruleBase;

    /** Application data which is associated with this memory. */
    private final Map applicationData = new HashMap();

    /** Object-to-handle mapping. */
    private final Map handles = new IdentityMap();

    /**
     * Leaps algorithm stack . TreeSet is as valid as ArrayList or Stack class
     * but can be faster for removing tokens for retracted/modified facts
     */
    TreeSet stack = new TreeSet(new Comparator() {
        public int compare(Object o1, Object o2) {
            LeapsToken token1 = (LeapsToken) o1;
            LeapsToken token2 = (LeapsToken) o2;
            // negatives are behind positive
            int ret = token2.getTokenType() - token1.getTokenType();
            if (ret == 0) {
                ret = (int) (token1.getDominantFactHandle().getId() - token2
                        .getDominantFactHandle().getId());
            }
            return ret;
        }
    });

    // to store facts to cursor over it
    private final Hashtable tables = new Hashtable();

    // to store negated facts to cursor over it in negative condition search
    private final Hashtable shadowTables = new Hashtable();

    /** The eventSupport */
    private final LeapsWorkingMemoryEventSupport workingMemoryEventSupport = new LeapsWorkingMemoryEventSupport(
            this);

    public LeapsWorkingMemory(LeapsRuleBase ruleBase) {
        this.ruleBase = ruleBase;
        this.runningCounter = 0;
    }

    /**
     * event support
     * @param listener
     */
    public void addEventListener(LeapsWorkingMemoryEventListener listener) {
        this.workingMemoryEventSupport.addEventListener(listener);
    }

    public void removeEventListener(LeapsWorkingMemoryEventListener listener) {
        this.workingMemoryEventSupport.removeEventListener(listener);
    }

    public List getWorkingMemoryEventListeners() {
        return this.workingMemoryEventSupport.getEventListeners();
    }

    protected synchronized long getNextId() {
        this.runningCounter++;
        return this.runningCounter;
    }

    public LeapsFactHandle assertObject(Object object) {
        synchronized (lock) {
            LeapsFactHandle factHandle = new LeapsFactHandle(this, object);
            // System.out.println("==> " + factHandle);
            // determine what classes it belongs to put it into the "table" on
            // class name key
            List tablesList = this.getLeapsFactTablesList(object.getClass());
            for (Iterator it = tablesList.iterator(); it.hasNext();) {
                // adding fact to container
                ((LeapsFactTable) it.next()).add(factHandle);
            }

            this.handles.put(object, factHandle);

            LeapsToken token = new LeapsToken(factHandle, object,
                    LeapsToken.ASSERTED);
            System.out.println("Pushing token on stack: "+object.getClass());
            pushTokenOnStack(token);

            // event support
            this.workingMemoryEventSupport.fireObjectAsserted(factHandle);

            return factHandle;
        }
    }

    /**
     * @see WorkingMemory
     */
    // public Object getObject(LeapsFactHandle factHandle) {
    // return factHandle.object;
    // // return this.getLeapsFactTable(this.rootClass).get(factHandle);
    // }
    //
    public List getObjects(Class objectClass)
            throws LeapsTableOutOfBoundException {
        List list = new LinkedList();
        for (Iterator it = this.getLeapsFactTable(objectClass).iterator(); it
                .hasNext();) {
            list.add(it.next());

        }

        return list;
    }

    /**
     * @see WorkingMemory
     */
    public LeapsFactHandle getFactHandle(Object object)
            throws LeapsNoSuchFactHandleException {
        LeapsFactHandle factHandle = (LeapsFactHandle) this.handles.get(object);

        if (factHandle == null) {
            throw new LeapsNoSuchFactHandleException(object);
        }

        return factHandle;
    }

    /**
     * generates or just return List of internal tables that correspond a class
     * can be used to generate tables
     * 
     * @return
     */
    public List getLeapsFactTablesList(Class c) {
        ArrayList list = new ArrayList();
        Class bufClass = c;
        while (bufClass != null) {
            //
            list.add(this.getLeapsFactTable(bufClass));
            // and get the next class on the list
            bufClass = bufClass.getSuperclass();
        }
        return list;
    }

    public List getLeapsShadowFactTablesList(Class c) {
        ArrayList list = new ArrayList();
        Class bufClass = c;
        while (bufClass != null) {
            //
            list.add(this.getLeapsShadowFactTable(bufClass));
            // and get the next class on the list
            bufClass = bufClass.getSuperclass();
        }
        return list;
    }

    public LeapsFactHandle retractObject(Object object)
            throws LeapsNoSuchFactHandleException {
        return this.retractFact(this.getFactHandle(object));
    }

    public LeapsFactHandle retractFact(LeapsFactHandle factHandle)
            throws LeapsNoSuchFactHandleException {
        synchronized (lock) {
            Object object = factHandle.object;
            LeapsFactHandle retractedFactHandle = new LeapsFactHandle(this,
                    object);
            // System.out.println("<== " + factHandle);
            List tablesList = null;
            // remove fact from all tables
            tablesList = this.getLeapsFactTablesList(object.getClass());
            for (Iterator it = tablesList.iterator(); it.hasNext();) {
                // removing fact to container
                ((LeapsFactTable) it.next()).remove(factHandle);
            }
            // remove fact from handles
            this.handles.remove(object);

            // add fact to all shadow tables
            tablesList = this.getLeapsShadowFactTablesList(object.getClass());
            for (Iterator it = tablesList.iterator(); it.hasNext();) {
                // adding fact to container
                ((LeapsFactTable) it.next()).add(retractedFactHandle);
            }
            // remove it from stack
            this.removeTokenFromStack(new LeapsToken(factHandle, object,
                    LeapsToken.ASSERTED));
            // put the new one back on stack
            LeapsToken token = new LeapsToken(retractedFactHandle, object,
                    LeapsToken.RETRACTED);
            this.pushTokenOnStack(token);

            // event support
            this.workingMemoryEventSupport.fireObjectRetracted(factHandle);

            return retractedFactHandle;
        }
    }

    public LeapsFactHandle modifyObject(Object object)
            throws LeapsNoSuchFactHandleException {
        return this.modifyFact(this.getFactHandle(object), object);
    }

    public LeapsFactHandle modifyFact(LeapsFactHandle factHandle, Object object)
            throws LeapsNoSuchFactHandleException {
        synchronized (lock) {
            this.retractFact(factHandle);
            return this.assertObject(object);
        }
    }

    public boolean isStackEmpty() {
        return this.stack.isEmpty();
    }

    public LeapsToken getTopTokenFromStack() {
        return (LeapsToken) this.stack.last();
    }

    /**
     * 
     * @return
     */

    protected void popTopTokenFromStack(LeapsToken token) {
        this.stack.remove(token);
    }

    private void removeTokenFromStack(LeapsToken token) {
        // add whatever additional info for token that you need : rules, ces
        // etc.
        this.stack.remove(token);
    }

    private void pushTokenOnStack(LeapsToken token) {
        // add whatever additional info for token that you need : rules, ces
        // etc.
        this.stack.add(token);
    }

    // getting shadow table
    // no need to create one. it would be created with the regular one
    // shadow is never hit before regular
    public LeapsFactTable getLeapsShadowFactTable(Class c) {
        return (LeapsFactTable) this.shadowTables.get(c);
    }

    // regular table
    public LeapsFactTable getLeapsFactTable(Class c) {
        LeapsFactTable table;
        if (this.tables.containsKey(c)) {
            table = (LeapsFactTable) this.tables.get(c);
        } else {
            table = new LeapsFactTable(this.ruleBase.getConflictResolver()
                    .getFactConflictResolver(), this.ruleBase
                    .getConflictResolver().getRuleConflictResolver());
            this.tables.put(c, table);
            // shadow tables created here
            this.shadowTables.put(c, new LeapsFactTable(this.ruleBase
                    .getConflictResolver().getFactConflictResolver(),
                    this.ruleBase.getConflictResolver()
                            .getRuleConflictResolver()));
        }

        return table;
    }

    protected void addRule(CororLeapsRule rule) {
        long ruleId = this.getNextId();
        ArrayList list;
        // positive
        for (int i = 0; i < rule.getNumberOfParameterDeclarations(); i++) {
            this.getLeapsFactTable(rule.getParameterDeclarationType(i))
                    .addPositiveRule(new LeapsRuleHandle(ruleId, rule, i));
        }
        // negative
        list = (ArrayList) rule.getNegativeConditions();
        for (int i = 0; i < list.size(); i++) {
            LeapsNegativeCondition negativeCondition = (LeapsNegativeCondition) list
                    .get(i);
            this.getLeapsFactTable(negativeCondition.getType())
                    .addNegativeRule(new LeapsRuleHandle(ruleId, rule, i));
        }
    }

    public void propertyChange(PropertyChangeEvent event) {
        Object object = event.getSource();

        try {
            modifyFact(getFactHandle(object), object);
        } catch (LeapsNoSuchFactHandleException e) {
            // Not a fact so unable to process the chnage event
        }
    }

    /**
     * main loop
     * 
     */
    public void fireAllRules() throws Exception {
        LeapsToken token;
        LeapsRuleHandle ruleHandle;
        LeapsFactTable factTable;
        boolean done;
        while (!this.isStackEmpty()) {

            token = this.getTopTokenFromStack();
            factTable = this.getLeapsFactTable(token.getDominantFactObject()
                    .getClass());
            System.out.println("Token's class: "+token.getDominantFactObject()
                    .getClass());
            // ok. now we have token, dominant fact and rules and ready to
            // seek
            done = false;

            while (!done) {
                if (!token.isResume()) {
                    if (factTable.hasNextRuleHandle(token)) {

                        ruleHandle = factTable.getNextRuleHandle(token);
                        System.out.println("Current RuleHandle "+ruleHandle);
                        token.setCurrentRuleHandle(ruleHandle);
                        
                    } else {
                        this.popTopTokenFromStack(token);
                        done = true;
                    }
                } else {
                	System.out.println("Resuming with Rule Handle: "+token.getCurrentRuleHandle());
                    ruleHandle = token.getCurrentRuleHandle();
                }
                try {
                    while (!done) {
                        try {
                            token.getCurrentRuleHandle().getRule().seek(this,
                                    token);
                            token.setResume(true);
                            done = true;

                            // event support
                            this.workingMemoryEventSupport.fireRuleFired(token);

                        } catch (LeapsNoMatchesFoundException ex) {
                            throw (ex);
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("exception - " + e);
                        }
                    }
                } catch (LeapsNoMatchesFoundException ex) {
                    token.setResume(false);
                    // do nothing just get the next rule to process
                }
            }
        }
    }

    public void dump() throws LeapsTableOutOfBoundException {
        Object key;
        System.out.println("Working memory");
        System.out.println("Fact Tables by types:");
        for (Enumeration e = this.tables.keys(); e.hasMoreElements();) {
            key = e.nextElement();
            System.out.println("******************   " + key);
            ((LeapsFactTable) this.tables.get(key)).dump();
        }
        System.out.println("Shadow Fact Tables by types:");
        for (Enumeration e = this.shadowTables.keys(); e.hasMoreElements();) {
            key = e.nextElement();
            System.out.println("******************   " + key);
            ((LeapsFactTable) this.shadowTables.get(key)).dump();
        }
        System.out.print("Stack:\n");
        for (Iterator it = stack.iterator(); it.hasNext();) {
            System.out.println("\t" + it.next());
        }
    }

    public void dump(Object objectFrom, Object objectAt, Object objectTo)
            throws LeapsTableOutOfBoundException {
        Object key;
        System.out.println("Working memory");
        System.out.println("Fact Tables by types:");
        for (Enumeration e = tables.keys(); e.hasMoreElements();) {
            key = e.nextElement();
            System.out.println("******************   " + key);
            ((LeapsFactTable) tables.get(key)).dump(objectFrom, objectAt,
                    objectTo);
        }
    }
}
