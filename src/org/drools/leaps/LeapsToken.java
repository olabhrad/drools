package org.drools.leaps;

import java.util.ArrayList;


/**
 * 
 * @author ab this class will reside on stack and care all links to other things
 * 
 */
public class LeapsToken {
    public final static int ASSERTED = 0;

    public final static int RETRACTED = 1;

    final int tokenType;

    final LeapsFactHandle factHandle;

    final Object object;

    LeapsRuleHandle currentRuleHandle = null;

    private ArrayList currentIterationState = new ArrayList();

    boolean resume = false;

    public LeapsToken(LeapsFactHandle factHandle, Object object, int tokenType) {
        this.factHandle = factHandle;
        this.object = object;
        this.tokenType = tokenType;
    }

    public LeapsFactHandle getFactHandle(int idx) {
        return (LeapsFactHandle) this.currentIterationState.get(idx);
    }

    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (!(that instanceof LeapsToken))
            return false;
        return this.factHandle.getId() == ((LeapsToken) that).factHandle
                .getId()
                && this.tokenType == ((LeapsToken) that).getTokenType();

    }

    public int hashCode() {
        return (int) this.factHandle.getId();
    }

    public void set(int idx, LeapsFactHandle factHandle) {
        this.currentIterationState.set(idx, factHandle);
    }

    public LeapsFactHandle getDominantFactHandle() {
        return this.factHandle;
    }

    public Object getDominantFactObject() {
        return this.object;
    }

    public LeapsRuleHandle getCurrentRuleHandle() {
        return this.currentRuleHandle;
    }

    public void setCurrentRuleHandle(LeapsRuleHandle currentRuleHandle) {
        this.currentRuleHandle = currentRuleHandle;

        if (!resume) {
            this.currentIterationState.clear();
            for (int i = 0; i < this.currentRuleHandle.getRule().getNumberOfParameterDeclarations(); i++) {
                this.currentIterationState.add(i, this.factHandle);
            }
        }
    }

    public int getTokenType() {
        return this.tokenType;
    }

    public boolean isResume() {
        return this.resume;
    }

    public void setResume(boolean resume) {
        this.resume = resume;
    }

    public String toString() {
        String ret = "TOKEN ["
                + ((this.tokenType == LeapsToken.ASSERTED) ? "ASSERTED"
                        : "RETRACTED") + ":: " + factHandle + "]\n"
                + "\tRULE : " + this.currentRuleHandle + "\n";
        if (this.currentIterationState != null) {
            for (int i = 0; i < currentIterationState.size(); i++) {
                ret = ret
                        + ((i == this.currentRuleHandle.getCePosition()) ? "***"
                                : "") + "\t" + i + " -> "
                        + this.currentIterationState.get(i) + "\n";
            }
        }
        return ret;
    }
}
