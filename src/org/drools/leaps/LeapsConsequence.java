package org.drools.leaps;

public interface LeapsConsequence {
	/**
	 * Execute the consequence for the rule
	 */
	void invoke() throws Exception;
}
