package org.drools.leaps.rule;

/*
 * $Id: RuleSet.java,v 1.1 2005/07/26 01:06:31 mproctor Exp $
 *
 * Copyright 2001-2003 (C) The Werken Company. All Rights Reserved.
 *
 * Redistribution and use of this software and associated documentation
 * ("Software"), with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * 1. Redistributions of source code must retain copyright statements and
 * notices. Redistributions must also contain a copy of this document.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name "drools" must not be used to endorse or promote products derived
 * from this Software without prior written permission of The Werken Company.
 * For written permission, please contact bob@werken.com.
 *
 * 4. Products derived from this Software may not be called "drools" nor may
 * "drools" appear in their names without prior written permission of The Werken
 * Company. "drools" is a trademark of The Werken Company.
 *
 * 5. Due credit should be given to The Werken Company. (http://werken.com/)
 *
 * THIS SOFTWARE IS PROVIDED BY THE WERKEN COMPANY AND CONTRIBUTORS ``AS IS''
 * AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE WERKEN COMPANY OR ITS CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

import ie.tcd.cs.nembes.coror.leaps.CororLeapsRule;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.Rule;

import java.util.Hashtable;

import org.drools.leaps.LeapsRuleBase;

/**
 * Collection of related <code>Rule</code>s.
 * 
 * @see Rule
 * 
 * @author <a href="mail:bob@werken.com">bob mcwhirter </a>
 * 
 * @version $Id: RuleSet.java,v 1.1 2005/07/26 01:06:31 mproctor Exp $
 */
public class LeapsRuleSet { // extends RuleSet {
    /** Name of the ruleset. */
    private String name;

    private LeapsRuleBase ruleBase;

    /** Ordered list of all <code>Rules</code> in this <code>RuleSet</code>. */
    private Hashtable rules;

    private Importer importer;

    // ------------------------------------------------------------
    // Constructors
    // ------------------------------------------------------------

    /**
     * Construct.
     * 
     * @param name
     *            The name of this <code>RuleSet</code>.
     * @param ruleBaseContext
     */
    public LeapsRuleSet(String name, LeapsRuleBase ruleBase)

    {
//        super(name, ruleBase.getRuleBaseContext());
        this.ruleBase = ruleBase;
        this.rules = new Hashtable();
    }

    /**
     * Add a <code>Rule</code> to this <code>RuleSet</code>.
     * 
     * @param rule
     *            The rule to add.
     * 
     * @throws DuplicateRuleNameException
     *             If the <code>Rule</code> attempting to be added has the
     *             same name as another previously added <code>Rule</code>.
     * @throws InvalidRuleException
     *             If the <code>Rule</code> is not valid.
     */
    public void addRule(CororLeapsRule rule) throws LeapsDuplicateRuleNameException,
            LeapsInvalidRuleException {
        rule.checkValidity();

        if (this.rules.containsKey(rule.getName())) {
            throw new LeapsDuplicateRuleNameException(this, rule);
        }

        this.rules.put(rule.getName(), rule);
        // assign id from rulebase/working memory

        // so far no need to do that. will see

        
        // added to all working memories via rulebase
        this.ruleBase.addRule(this, rule);

        rule.setImporter(this.importer);
    }

    /**
     * Retrieve a <code>Rule</code> by name.
     * 
     * @param name
     *            The name of the <code>Rule</code> to retrieve.
     * 
     * @return The named <code>Rule</code>, or <code>null</code> if not
     *         such <code>Rule</code> has been added to this
     *         <code>RuleSet</code>.
     */
    public CororLeapsRule getLeapsRule(String name) {
        return (CororLeapsRule) this.rules.get(name);
    }

    /**
     * Determine if this <code>RuleSet</code> contains a <code>Rule</code
     *  with the specified name.
     *
     *  @param name The name of the <code>Rule</code>.
     *
     *  @return <code>true</code> if this <code>RuleSet</code> contains a
     *          <code>Rule</code> with the specified name, else <code>false</code>.
     */
    public boolean containsRule(String name) {
        return this.rules.containsKey(name);
    }

    public LeapsRuleBase getRuleBase() {
        return this.getRuleBase();
    }
    public Importer getImporter() {
        return this.importer;
    }

    public void setImporter(Importer importer) {
        this.importer = importer;
    }

    public String getName () {
        return this.name;
    }
}
