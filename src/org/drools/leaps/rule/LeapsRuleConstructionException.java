package org.drools.leaps.rule;

/**
 * Base exception for errors during <code>Rule</code> construction.
 *
 */
public class LeapsRuleConstructionException extends Exception
{
    /**
     * @see java.lang.Exception#Exception()
     */
    LeapsRuleConstructionException()
    {
        super();
    }

    /**
     * @see java.lang.Exception#Exception(String message)
     */
    LeapsRuleConstructionException( String message )
    {
        super( message );
    }

    /**
     * @see java.lang.Exception#Exception(String message, Throwable cause)
     */
    LeapsRuleConstructionException( String message, Throwable cause )
    {
        super( message, cause );
    }

    /**
     * @see java.lang.Exception#Exception(Throwable cause)
     */
    LeapsRuleConstructionException( Throwable cause )
    {
        super( cause );
    }
}
