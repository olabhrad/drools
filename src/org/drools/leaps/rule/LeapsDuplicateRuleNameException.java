package org.drools.leaps.rule;

import ie.tcd.cs.nembes.coror.leaps.CororLeapsRule;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.Rule;

/**
 * Indicates an attempt to add a <code>Rule</code> to a <code>RuleSet</code>
 * that already contains a <code>Rule</code> with the same name.
 *
 * @see Rule
 * @see LeapsRuleSet
 *
 * @author <a href="mailto:bob@eng.werken.com">bob mcwhirter </a>
 */
public class LeapsDuplicateRuleNameException extends LeapsRuleConstructionException
{
    /** The rule-set. */
    private LeapsRuleSet ruleSet;

    /** The member rule. */
    private CororLeapsRule originalRule;

    /**
     * @see java.lang.Exception#Exception()
     *
     * @param ruleSet
     *            The <code>RuleSet</code>.
     * @param originalRule
     *            The <code>Rule</code> already in the <code>RuleSet</code>.
     * @param conflictingRule
     *            The new, conflicting <code>Rule</code>.
     */
    public LeapsDuplicateRuleNameException(LeapsRuleSet ruleSet,
    		CororLeapsRule originalRule)
    {
        super( createMessage( ruleSet, originalRule ) );
        this.ruleSet = ruleSet;
        this.originalRule = originalRule;
            }

    /**
     * @see java.lang.Exception#Exception(Throwable cause)
     *
     * @param ruleSet
     *            The <code>RuleSet</code>.
     * @param originalRule
     *            The <code>Rule</code> already in the <code>RuleSet</code>.
     * @param conflictingRule
     *            The new, conflicting <code>Rule</code>.
     */
    public LeapsDuplicateRuleNameException(LeapsRuleSet ruleSet,
    		CororLeapsRule originalRule,
                                      Throwable cause)
    {
        super( createMessage( ruleSet, originalRule ), cause );
        this.ruleSet = ruleSet;
        this.originalRule = originalRule;
     
    }

    /**
     * Retrieve the <code>RuleSet</code>.
     *
     * @return The <code>RuleSet</code>.
     */
    public LeapsRuleSet getRuleSet()
    {
        return this.ruleSet;
    }

    /**
     * Retrieve the original <code>Rule</code> in the <code>RuleSet</code>.
     *
     * @return The <code>Rule</code>.
     */
    public CororLeapsRule getOriginalRule()
    {
        return this.originalRule;
    }

     private static String createMessage(LeapsRuleSet ruleSet, CororLeapsRule rule) {
        return "Rule-set "
            + ((ruleSet.getName() != null) ? ruleSet.getName() : "<no-name>")
            + " already contains rule with name "
            + rule.getName();
    }
}
