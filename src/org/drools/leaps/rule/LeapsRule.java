package org.drools.leaps.rule;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.drools.leaps.LeapsCondition;
import org.drools.leaps.LeapsConsequence;
import org.drools.leaps.LeapsDeclaration;
import org.drools.leaps.LeapsDeclarationImpl;
import org.drools.leaps.LeapsFactHandle;
import org.drools.leaps.LeapsFactTable;
import org.drools.leaps.LeapsNegativeCondition;
import org.drools.leaps.LeapsNoMatchesFoundException;
import org.drools.leaps.LeapsToken;
import org.drools.leaps.LeapsWorkingMemory;
import org.drools.leaps.util.LeapsTableIterator;

/**
 * Base class for all rules instantiations. Does actual hunt for matches base on
 * positive and negative conditions
 * 
 * @author ab
 * 
 */
public class LeapsRule {
    /** The parent ruleSet */
    private LeapsRuleSet ruleSet;

    /** Formal parameter declarations. */
    private final ArrayList declarations = new ArrayList();

    /** Name of the rule. */
    private final String name;

    /** Documentation. */
    private String documentation = "no documentation present";

    /** Salience value. */
    private int salience = 0;

    /** Consequence. */
    private LeapsConsequence consequence = null;

    private Importer importer;

    /** Load order in RuleSet */
    private long loadOrder;

    /**
     * Condition - positive. stored at the "top" declaration level it checked
     * when iterator gets to the "top" declation position
     */
    private final Hashtable positiveConditions = new Hashtable();

    /** Negative conditions map by declaration. */
    private final List negativeConditions = new ArrayList();

    private ArrayList iterators = new ArrayList();

    public LeapsRule(String name) {
        this.name = name;
    }

    /**
     * Searches <code>Working Memory</code> for facts that would satisfy
     * <code>Conditions</code>.
     * 
     * @param memory
     *            The Working Memory.
     * @param token
     *            The token that cares information about dominant fact and
     *            current state of processing for this fact.
     */
    @SuppressWarnings("unchecked")
	public void seek(LeapsWorkingMemory memory, LeapsToken token)
            throws LeapsNoMatchesFoundException, Exception,
            LeapsInvalidRuleException {
        int numberOfDeclarations = declarations.size();
        // getting iterators first
        for (int i = 0; i < numberOfDeclarations; i++) {
            // there is no dominant fact for negative based searches
            if (token.getTokenType() == LeapsToken.ASSERTED
                    && i == token.getCurrentRuleHandle().getCePosition()) {
                this.iterators.set(i, memory.getLeapsFactTable(
                        ((LeapsDeclarationImpl) declarations.get(i)).getType())
                        .baseFactIterator(token.getDominantFactHandle()));
                System.out.println("fixed: type"+i+((LeapsDeclarationImpl) declarations.get(i)).getType());
                System.out.println(((LeapsTableIterator)this.iterators.get(i)).peekNext());                
            } else {
                this.iterators.set(i, memory.getLeapsFactTable(
                        ((LeapsDeclarationImpl) declarations.get(i)).getType())
                        .iterator(
                                token.getDominantFactHandle(),
                                (token.isResume() ? token.getFactHandle(i)
                                        : token.getDominantFactHandle())));
                System.out.println("type"+i+((LeapsDeclarationImpl) declarations.get(i)).getType());
    //            Iterator currentIterator = (LeapsTableIterator) iterators.get(i);
     //           while(currentIterator.hasNext()){
       //         	System.out.println(currentIterator.next());
         //       }
            }
            // init results
            ((LeapsDeclarationImpl) this.declarations.get(i))
                    .setIterator((LeapsTableIterator) this.iterators.get(i));
        }
        // check if any iterators are empty to abort
        // check if we resume and any facts disappeared
        boolean someIteratorsEmpty = false;
        boolean doReset = false;
        boolean skip = token.isResume();
        LeapsTableIterator currentIterator;
        for (int i = 0; i < numberOfDeclarations && !someIteratorsEmpty; i++) {
            currentIterator = (LeapsTableIterator) iterators.get(i);
            if (currentIterator.isEmpty()) {
                someIteratorsEmpty = true;
            } else {
                if (!doReset) {
                    if (skip
                            && !currentIterator.peekNext().equals(token.getFactHandle(i))) {
                        skip = false;
                        doReset = true;
                    }
                } else {
                    currentIterator.reset();
                }
            }

        }
        // check if one of them is empty and immediate return
        if (someIteratorsEmpty) {
            throw new LeapsNoMatchesFoundException(
                    "some of tables do not have facts \n" + token);
        }
      
        
        // iterating
        int jj = 0;
        boolean found = false;
        boolean done = false;
        while (!done) {
            currentIterator = (LeapsTableIterator) iterators.get(jj);
            if (!currentIterator.hasNext()) {
                if (jj == 0) {
                    done = true;
                } else {
                    //                    
                    currentIterator.reset();
                    jj = jj - 1;
                    if (skip) {
                        skip = false;
                    }
                }
            } else {
                currentIterator.next();
                // check if match found
                if (this.evaluatePositiveConditions(jj)) {
                    // start iteratating next iterator
                    // or for the last one check negative conditions and fire
                    // consequence
                    if (jj == (numberOfDeclarations - 1)) {
                        if (!skip) {
                            // check for negative conditions
                            if (this.evaluateNegativeCondition(memory, token)) {
                                done = true;
                                found = true;
                                // System.out.println("FIRE: " + token);
                                this.consequence.invoke();
                                // store current state if to resume iterations
                                // here
                                for (int l = 0; l < iterators.size(); l++) {
                                    token.set(l,
                                         (LeapsFactHandle) ((LeapsTableIterator) iterators
                                            .get(l)).current());
                                }
                            }
                        } else {
                            skip = false;
                        }
                    } else {
                        jj = jj + 1;
                    }
                } else {
                    if (skip) {
                        skip = false;
                    }
                }
            }
        }
        if (!found) {
            throw new LeapsNoMatchesFoundException(
                    "iteration did not find anything");
        }
    }

    /**
     * 
     * Check if any conditions with max value of declaration index at this
     * position (<code>index</code>) are satisfied
     * 
     * @param index
     *            Position of the iterator that needs condition checking
     * @return success Indicator if all conditions at this position were
     *         satisfied.
     * @throws Exception
     */
    private boolean evaluatePositiveConditions(int index) throws Exception {
        boolean ret = true;
        if (this.positiveConditions.containsKey(new Integer(index))) {
            ArrayList conds = (ArrayList) this.positiveConditions
                    .get(new Integer(index));
            for (int i = 0; i < conds.size() && ret; i++) {

                	System.out.println(i);
                ret = ((LeapsCondition) conds.get(i)).isAllowed();
            }
        }
        return ret;
    }

    /**
     * Check if any of the negative conditions are satisfied, success when none
     * found
     * 
     * @param memory
     * @param token
     * @return success
     * @throws Exception
     */
    private boolean evaluateNegativeCondition(LeapsWorkingMemory memory,
            LeapsToken token) throws Exception {
        boolean notFound = true;
        // checking retracted negative that becomes positive
        if (token.getTokenType() == LeapsToken.RETRACTED) {
            notFound = ((LeapsNegativeCondition) this.negativeConditions
                    .get(token.getCurrentRuleHandle().getCePosition()))
                    .isAllowed(token.getDominantFactHandle().getObject());
            // .isAllowed(token.getDominantFactHandle().object);

        }
        if (notFound) {
            // checking all negative
            LeapsNegativeCondition negativeCondition;
            LeapsTableIterator tableIterator;
            LeapsFactTable table;
            for (Iterator it = this.negativeConditions.iterator(); it.hasNext()
                    && notFound;) {
                negativeCondition = (LeapsNegativeCondition) it.next();
                // 1. starting with regular tables
                // scan the whole table
                table = memory.getLeapsFactTable(negativeCondition.getType());
                tableIterator = table.iterator(table.getHeadObject());
                while (tableIterator.hasNext() && notFound) {
                    notFound = !negativeCondition
                            .isAllowed(((LeapsFactHandle) tableIterator.next())
                                    .getObject());
                    // .isAllowed(((LeapsFactHandle)
                    // tableIterator.next()).object);
                }
                if (notFound) {
                    // 2. checking shadow tables
                    // scan only higher facts
                    table = memory.getLeapsShadowFactTable(negativeCondition
                            .getType());
                    tableIterator = table.iterator(table.getHeadObject(), table
                            .getHeadObject(), token.getDominantFactHandle());
                    while (tableIterator.hasNext() && notFound) {
                        notFound = !negativeCondition
                                .isAllowed(((LeapsFactHandle) tableIterator
                                        .next()).getObject());
                        // .next()).object);
                    }
                }
            }
        }
        return notFound;
    }

    /**
     * Set the documentation.
     * 
     * @param documentation -
     *            The documentation.
     */
    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    /**
     * Retrieve the documentation.
     * 
     * @return The documentation or <code>null</code> if none.
     */
    /**
     * @return
     */
    public String getDocumentation() {
        return this.documentation;
    }

    public void checkValidity() throws LeapsInvalidRuleException {
        if (this.declarations.isEmpty()) {
            throw new LeapsNoParameterDeclarationException(this);
        }

        if (this.consequence == null) {
            throw new LeapsNoConsequenceException(this);
        }
    }

    public LeapsRuleSet getRuleSet() {
        return this.ruleSet;
    }

    /**
     * Retrieve the name of this rule.
     * 
     * @return The name of this rule.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Retrieve the <code>Rule</code> salience.
     * 
     * @return The salience.
     */
    public int getSalience() {
        return this.salience;
    }

    /**
     * Set the <code>Rule<code> salience.
     *
     * @param salience The salience.
     */
    public void setSalience(int salience) {
        this.salience = salience;
    }

    public long getLoadOrder() {
        return loadOrder;
    }

    void setLoadOrder(long loadOrder) {
        this.loadOrder = loadOrder;
    }

    public Importer getImporter() {
        return this.importer;
    }

    public void setImporter(Importer importer) {
        this.importer = importer;
    }

    /**
     * Adds the <code>Declaration</code> for one of the rule variables.
     * 
     * @param identifier
     *            The declaration identifier.
     * @param class
     *            The variable type.
     * @return The declaration.
     */
    @SuppressWarnings("unchecked")
	public LeapsDeclaration addParameterDeclaration(String identifier, Class c)
            throws LeapsInvalidRuleException {
    	System.out.println("ID: "+identifier);
        if (this.existsParameterDeclaration(identifier)) {
            throw new LeapsInvalidRuleException(
                    "Attempt to add existing param name", this);
        }
        // add declaration
        this.declarations.add(this.declarations.size(),
                new LeapsDeclarationImpl(identifier, c, this.declarations
                        .size()));
        // add iterator position and its position
        this.iterators.add(this.iterators.size(), null);
        ((LeapsDeclarationImpl) this.declarations
                .get(this.declarations.size() - 1))
                .setIterator((LeapsTableIterator) this.iterators
                        .get(this.iterators.size() - 1));
        // return
        return (LeapsDeclarationImpl) this.declarations.get(this.declarations
                .size() - 1);
    }

    public void addNegativeCondition(LeapsNegativeCondition condition)
            throws LeapsInvalidRuleException {
        this.negativeConditions.add(condition);
    }

    private boolean existsParameterDeclaration(String identifier) {
        if (this.getNumberOfParameterDeclarations() > 0) {
            for (int i = 0; i < this.declarations.size(); i++) {
                if (((LeapsDeclarationImpl) this.declarations.get(i))
                        .getIdentifier().equals(identifier)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param index
     * @return
     */
    public Class getParameterDeclarationType(int index) {
        return ((LeapsDeclarationImpl) this.declarations.get(index)).getType();
    }

    /**
     * @return
     */
    public int getNumberOfParameterDeclarations() {
        return this.declarations.size();
    }

    /**
     * @param pos
     * @return
     */
    public LeapsNegativeCondition getNegativeCondition(int pos) {
        return (LeapsNegativeCondition) this.negativeConditions.get(pos);
    }

    /**
     * @return
     */
    public List getNegativeConditions() {
        return this.negativeConditions;
    }

    /**
     * Set the <code>Consequence</code> that is associated with the successful
     * match of this rule.
     * 
     * @param consequence
     *            The <code>Consequence</code> to attach to this
     *            <code>Rule</code>.
     */
    public void setConsequence(LeapsConsequence consequence) {
        this.consequence = consequence;
    }

    public LeapsConsequence getConsequence() {
        return this.consequence;
    }

    /**
     * Set the <code>Condition</code> that is associated with the successful
     * match of this rule.
     * 
     * @param condition
     *            The <code>condition</code> to attach to this
     *            <code>Rule</code>.
     */
    public void addCondition(LeapsCondition condition)
            throws LeapsInvalidRuleException {
        // determining conditionTopDeclarations
        int topDeclarationIndex = -1;
        int indexOf = -1;
        LeapsDeclaration requiredDeclartions[] = (LeapsDeclaration[]) condition
                .getRequiredDeclarations();
        for (int i = 0; i < requiredDeclartions.length; i++) {
            indexOf = this.declarations.indexOf(requiredDeclartions[i]);
            if (indexOf > topDeclarationIndex) {
                topDeclarationIndex = indexOf;
            }
        }
        if (topDeclarationIndex == -1) {
            throw new LeapsInvalidRuleException(
                    "No matching declarations for condition", this);
        }
        ArrayList conds = null;
        if (this.positiveConditions
                .containsKey(new Integer(topDeclarationIndex))) {
            conds = (ArrayList) this.positiveConditions.get(new Integer(
                    topDeclarationIndex));
        } else {
            conds = new ArrayList();
        }
        conds.add(condition);
        this.positiveConditions.put(new Integer(topDeclarationIndex), conds);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.name;
    }
}
