package org.drools.leaps;

import java.util.Comparator;
import java.util.TreeMap;

import org.drools.leaps.util.LeapsTable;

class LeapsRuleTable extends LeapsTable {
	public LeapsRuleTable(Comparator ruleConflictResolver) {
		this.setMap(new TreeMap(ruleConflictResolver));
	}
}
