package org.drools.leaps;

public class LeapsHandle {
	final protected Object object;

	private final long id;

	final private long timeStamp;

	public LeapsHandle(long id, Object object) {
		this.id = id;
		this.timeStamp = System.currentTimeMillis();
		this.object = object;
	}

	public long getId() {
		return this.id;
	}

	public long getTimeStamp() {
		return this.timeStamp;
	}

	public Object getObject() {
		return object;
	}

	public int hashCode() {
		return (int) this.getId();
	}
    
    public String toString() {
        return "id=" + this.id + " [" + this.object + "]";
    }
}
