package org.drools.leaps;

import java.util.Comparator;
import java.util.TreeMap;

import org.drools.leaps.util.LeapsTable;
import org.drools.leaps.util.LeapsTableIterator;
import org.drools.leaps.util.LeapsTableOutOfBoundException;

public class LeapsFactTable extends LeapsTable {
    private LeapsRuleTable positiveRules;

    private LeapsRuleTable negativeRules;

    public LeapsFactTable(Comparator factConflictResolver,
            Comparator ruleConflictResolver) {
        this.setMap(new TreeMap(factConflictResolver));
        positiveRules = new LeapsRuleTable(ruleConflictResolver);
        negativeRules = new LeapsRuleTable(ruleConflictResolver);
    }

    protected boolean hasNextRuleHandle(LeapsToken token)
            throws LeapsTableOutOfBoundException {
        return this.getRulesIterator(token).hasNext();

    }

    protected LeapsRuleHandle getNextRuleHandle(LeapsToken token)
            throws LeapsTableOutOfBoundException {
        return (LeapsRuleHandle) this.getRulesIterator(token).next();
    }

    private LeapsTableIterator getRulesIterator(LeapsToken token)
            throws LeapsTableOutOfBoundException {
        LeapsTableIterator ret;
        LeapsRuleTable rulesTable;
        if (token.getTokenType() == LeapsToken.ASSERTED) {
            rulesTable = this.positiveRules;
        } else {
            rulesTable = this.negativeRules;
        }

        if (token.getCurrentRuleHandle() == null) {
            ret = rulesTable.iterator();
        } else {
            ret = rulesTable.iterator(token.getCurrentRuleHandle());
            // skipping itself
            ret.next();
        }

        return ret;
    }

    public void addNegativeRule(LeapsRuleHandle ruleHandle) {
        this.negativeRules.add(ruleHandle);
    }

    public void addPositiveRule(LeapsRuleHandle ruleHandle) {
        this.positiveRules.add(ruleHandle);
    }

    public LeapsTableIterator baseFactIterator(LeapsFactHandle factHandle) {
        return new LeapsTableIterator(factHandle);
    }

    public void dump() throws LeapsTableOutOfBoundException {
        super.dump();
        System.out.println("POSITIVE RULES :");
        this.positiveRules.dump();
        System.out.println("NEGATIVE RULES :");
        this.negativeRules.dump();
    }
}
