package org.drools.leaps;

public abstract class LeapsNegativeCondition {
    private final Class type;

    public LeapsNegativeCondition(Class c) {
        this.type = c;
    }

    /**
     * 
     */
    abstract public boolean isAllowed(Object object) throws Exception;

    public Class getType() {
        return type;
    }

}
