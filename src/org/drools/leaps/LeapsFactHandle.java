package org.drools.leaps;


/**
 * 
 * @author ab class container for objects asserted into the system
 */
public class LeapsFactHandle extends LeapsHandle {
	/**
	 * actual object that is asserted to the system
	 * no getters just a direct access to speed things up 
	 */
//	final private Object object;
//
	public LeapsFactHandle(LeapsWorkingMemory workingMemory, Object object) {
		super(workingMemory.getNextId(), object);
//		this.object = object;
	}

	public boolean equals(Object that) {
		if (this == that)
			return true;
		if (!(that instanceof LeapsFactHandle))
			return false;
		return this.getId() == ((LeapsFactHandle) that).getId();

	}

	public String toString() {
		return "f-" + this.getId();// + " " + object.toString();
	}

//	public Object getObject() {
//		return object;
//	}
}
