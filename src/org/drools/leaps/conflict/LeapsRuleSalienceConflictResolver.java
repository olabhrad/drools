package org.drools.leaps.conflict;

import java.util.Comparator;

import org.drools.leaps.LeapsRuleHandle;

/**
 * <code>LeapsRuleConflictResolver</code> that uses the salience of rules to resolve
 * conflict.
 *
 * @see #getInstance
 */
public class LeapsRuleSalienceConflictResolver implements
Comparator {
    // ----------------------------------------------------------------------
    // Class members
    // ----------------------------------------------------------------------

    /** Singleton instance. */
    private static final LeapsRuleSalienceConflictResolver INSTANCE = new LeapsRuleSalienceConflictResolver();

    // ----------------------------------------------------------------------
    // Class methods
    // ----------------------------------------------------------------------

    /**
     * Retrieve the singleton instance.
     *
     * @return The singleton instance.
     */
    public static Comparator getInstance() {
        return INSTANCE;
    }

    // ----------------------------------------------------------------------
    // Constructors
    // ----------------------------------------------------------------------

    /**
     * Construct.
     */
    private LeapsRuleSalienceConflictResolver() {
        // intentionally left blank
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * @see LeapsRuleConflictResolver
     */
    public int compare(Object o1, Object o2) {
        return (-1) * LeapsAbstractConflictResolver.compare(((LeapsRuleHandle) o1).getRule()
                .getSalience(),
                ((LeapsRuleHandle) o2).getRule().getSalience());
    };
}
