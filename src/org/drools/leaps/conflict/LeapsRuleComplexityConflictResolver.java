package org.drools.leaps.conflict;

import java.util.Comparator;

import org.drools.leaps.LeapsRuleHandle;

/**
 * <code>LeapsRuleConflictResolver</code> that uses the number of conditions of rules to resolve
 * conflict.
 *
 * @see #getInstance
 */
public class LeapsRuleComplexityConflictResolver implements
Comparator {
    // ----------------------------------------------------------------------
    // Class members
    // ----------------------------------------------------------------------

    /** Singleton instance. */
    private static final LeapsRuleComplexityConflictResolver INSTANCE = new LeapsRuleComplexityConflictResolver();

    // ----------------------------------------------------------------------
    // Class methods
    // ----------------------------------------------------------------------

    /**
     * Retrieve the singleton instance.
     *
     * @return The singleton instance.
     */
    public static Comparator getInstance() {
        return INSTANCE;
    }

    // ----------------------------------------------------------------------
    // Constructors
    // ----------------------------------------------------------------------

    /**
     * Construct.
     */
    private LeapsRuleComplexityConflictResolver() {
        // intentionally left blank
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * @see LeapsRuleConflictResolver
     */
    public int compare(Object o1, Object o2) {
        return (-1) * LeapsAbstractConflictResolver.compare((((LeapsRuleHandle) o1).getRule()
                .getNumberOfParameterDeclarations()
                + ((LeapsRuleHandle) o1).getRule()
                        .getNegativeConditions().size())
                , (((LeapsRuleHandle) o2).getRule()
                        .getNumberOfParameterDeclarations()
                        + ((LeapsRuleHandle) o2).getRule()
                                .getNegativeConditions().size()));
        };
}
