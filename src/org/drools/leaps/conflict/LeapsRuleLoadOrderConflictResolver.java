package org.drools.leaps.conflict;

import java.util.Comparator;

import org.drools.leaps.LeapsRuleHandle;

/**
 * <code>LeapsRuleConflictResolver</code> that uses the load order of rules to
 * resolve conflict.
 * 
 * @see #getInstance
 */
public class LeapsRuleLoadOrderConflictResolver implements Comparator {
    // ----------------------------------------------------------------------
    // Class members
    // ----------------------------------------------------------------------

    /** Singleton instance. */
    private static final LeapsRuleLoadOrderConflictResolver INSTANCE = new LeapsRuleLoadOrderConflictResolver();

    // ----------------------------------------------------------------------
    // Class methods
    // ----------------------------------------------------------------------

    /**
     * Retrieve the singleton instance.
     * 
     * @return The singleton instance.
     */
    public static Comparator getInstance() {
        return INSTANCE;
    }

    // ----------------------------------------------------------------------
    // Constructors
    // ----------------------------------------------------------------------

    /**
     * Construct.
     */
    private LeapsRuleLoadOrderConflictResolver() {
        // intentionally left blank
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * @see LeapsRuleLoadOrderResolver
     */
    public int compare(Object o1, Object o2) {
        int ret = LeapsLoadOrderConflictResolver.getInstance().compare(o1, o2);
        if (ret == 0) {
            ret = (-1)
                    * LeapsAbstractConflictResolver.compare(
                            ((LeapsRuleHandle) o1).getCePosition(),
                            ((LeapsRuleHandle) o2).getCePosition());
        }
        return ret;
    };
}
