package org.drools.leaps.conflict;

import java.util.Comparator;

import org.drools.leaps.LeapsConflictResolver;

/**
 * Strategy for resolving conflicts amongst multiple rules.
 * 
 * <p>
 * Since a fact or set of facts may activate multiple rules, a
 * <code>ConflictResolutionStrategy</code> is used to provide priority
 * ordering of conflicting rules.
 * </p>
 */
public class LeapsDefaultConflictResolver extends
        LeapsCompositeConflictResolver {
    // ----------------------------------------------------------------------
    // Class members
    // ----------------------------------------------------------------------

    private static final Comparator[] FACT_CONFLICT_RESOLVERS = new Comparator[] { LeapsLoadOrderConflictResolver
            .getInstance() };

    private static final Comparator[] RULE_CONFLICT_RESOLVERS = new Comparator[] {
            LeapsRuleSalienceConflictResolver.getInstance(),
            LeapsRuleComplexityConflictResolver.getInstance(),
            LeapsRuleLoadOrderConflictResolver.getInstance()};

    /** Singleton instance. */
    private static final LeapsDefaultConflictResolver INSTANCE = new LeapsDefaultConflictResolver();

    // ----------------------------------------------------------------------
    // Class methods
    // ----------------------------------------------------------------------

    /**
     * Retrieve the singleton instance.
     * 
     * @return The singleton instance.
     */
    public static LeapsConflictResolver getInstance() {
        return INSTANCE;
    }

    /**
     * Setup a default ConflictResolver configuration
     */
    public LeapsDefaultConflictResolver() {
        super(FACT_CONFLICT_RESOLVERS, RULE_CONFLICT_RESOLVERS);
    }
}
