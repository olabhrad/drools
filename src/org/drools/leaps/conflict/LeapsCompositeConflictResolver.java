package org.drools.leaps.conflict;

import java.util.Comparator;

import org.drools.leaps.LeapsConflictResolver;

public class LeapsCompositeConflictResolver extends
        LeapsAbstractConflictResolver {
    private final Comparator[] factResolvers;

    private final Comparator[] ruleResolvers;

    public LeapsCompositeConflictResolver(
            Comparator[] factResolvers,
            Comparator[] ruleResolvers) {
        this.factResolvers = factResolvers;
        this.ruleResolvers = ruleResolvers;
    }

    public final Comparator getFactConflictResolver() {
        return new Comparator() {
            public int compare(Object o1, Object o2) {
                int ret = 0;
                if (o1 != o2) {
                    for (int i = 0; ret == 0 && i < factResolvers.length; ++i) {
                        ret = factResolvers[i].compare(o1, o2);
                    }
                }
                return ret;
            }
        };
    }

    public final Comparator getRuleConflictResolver() {
        return new Comparator() {
            public int compare(Object o1, Object o2) {
                int ret = 0;
                if (o1 != o2) {
                    for (int i = 0; ret == 0 && i < ruleResolvers.length; ++i) {
                        ret = ruleResolvers[i].compare(o1, o2);
                    }
                }
                return ret;
            }
        };
    }
}
