package org.drools.leaps.conflict;

import java.util.Comparator;

import org.drools.leaps.LeapsHandle;
/**
 * <code>LeapsRuleConflictResolver</code> that uses the load order of rules to
 * resolve conflict.
 * 
 * @see #getInstance
 */
public class LeapsLoadOrderConflictResolver implements
        Comparator {
    // ----------------------------------------------------------------------
    // Class members
    // ----------------------------------------------------------------------

    /** Singleton instance. */
    private static final LeapsLoadOrderConflictResolver INSTANCE = new LeapsLoadOrderConflictResolver();

    // ----------------------------------------------------------------------
    // Class methods
    // ----------------------------------------------------------------------

    /**
     * Retrieve the singleton instance.
     * 
     * @return The singleton instance.
     */
    public static Comparator getInstance() {
        return INSTANCE;
    }

    // ----------------------------------------------------------------------
    // Constructors
    // ----------------------------------------------------------------------

    /**
     * Construct.
     */
    private LeapsLoadOrderConflictResolver() {
        // intentionally left blank
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * @see LeapsRuleConflictResolver
     */
    public int compare(Object o1, Object o2) {
        return (-1)
                * LeapsAbstractConflictResolver.compare(((LeapsHandle) o1)
                        .getId(), ((LeapsHandle) o2).getId());
    };
}
