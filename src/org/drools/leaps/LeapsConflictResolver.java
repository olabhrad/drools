package org.drools.leaps;

import java.util.Comparator;

public interface LeapsConflictResolver {
    public Comparator getFactConflictResolver();

    public Comparator getRuleConflictResolver();
}
