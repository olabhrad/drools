package org.drools.leaps;

/**
 * Rule's positive condition
 * 
 * @author ab
 *
 */
public interface LeapsCondition {
	/**
	 * @return
	 * @throws Exception
	 */
	boolean isAllowed() throws Exception;
    /**
     * @return
     */
    public LeapsDeclaration[] getRequiredDeclarations();
}
